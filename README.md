### What is this repository for? ###

This repository is for a *personal project* of mine to create a **C++** **DirectX 11** rendering framework.

I don't really have any future intention for this framework, I just like making frameworks and learning new technologies and using new OO and Generic programming techniques.

I have a task list to give you an idea of the systems I'll be implementing/improving on over time: https://trello.com/b/shFCznsp/rendering

### How do I get set up? ###

Almost everything *should* work out of the box.

The project is a **Visual Studio 2013** project; I can't guarantee that it will work in 2012 as it may contain *C++14* code not supported by 2012.

#### Dependencies ####

* Windows 8.1 SDK

* ***DirectX ToolKit is provided and pre-compiled with only what is currently being used in the project.***

### Contribution guidelines ###

This is a personal project and I am not looking for or accepting for any contributors.