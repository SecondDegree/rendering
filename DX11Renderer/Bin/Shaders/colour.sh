//
//  (c) 2014 James Hannam
//
//  File Name   :   color.vs
//  Description :   A basic vertex shader.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Globals
cbuffer MatrixBuffer
{
	matrix g_World;
	matrix g_View;
	matrix g_Proj;
};

// Input Types
struct VSIn
{
	float3 posL : POSITION;
	float4 colour : COLOUR;
};

struct PSIn
{
	float4 posH : SV_POSITION;
	float4 colour : COLOUR;
};

// Functions
float4 LocalToClipSpace(float3 _PosL)
{
	float4 Position = float4(_PosL, 1.0f);

	float4 oPosH = mul(Position, g_World);
	oPosH = mul(oPosH, g_View);
	oPosH = mul(oPosH, g_Proj);

	return (oPosH);
}