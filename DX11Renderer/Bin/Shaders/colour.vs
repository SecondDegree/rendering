//
//  (c) 2014 James Hannam
//
//  File Name   :   color.vs
//  Description :   A basic vertex shader.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#include "colour.sh"

// Vertex Shader
PSIn
VS(VSIn input)
{
	PSIn output;
	
	output.posH = LocalToClipSpace(input.posL);

	output.colour = input.colour;

	return output;
}