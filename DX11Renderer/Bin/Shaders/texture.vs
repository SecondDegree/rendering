//
//  (c) 2014 James Hannam
//
//  File Name   :   texture.vs
//  Description :   A basic texturing vertex shader.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#include "texture.sh"

// Vertex Shader
PSIn
VS(VSIn input)
{
	PSIn output;
	
	output.posH = LocalToClipSpace(input.posL);

	output.texCoord = input.texCoord;

	return output;
}