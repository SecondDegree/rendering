//
//  (c) 2015 James Hannam
//
//  File Name   :   unlit.vs
//  Description :   The unlit shader helper.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Globals
cbuffer MatrixBuffer// : register(cb0)
{
	matrix g_World;
	matrix g_View;
	matrix g_Proj;
};

cbuffer ColourBuffer// : register(cb1)
{
	float4 g_Colour;
};

// Input Types
struct VSIn
{
	float3 posL : POSITION;
};

struct PSIn
{
	float4 posH : SV_POSITION;
};

// Functions
float4 LocalToClipSpace(float3 _PosL)
{
	float4 Position = float4(_PosL, 1.0f);

	float4 oPosH = mul(Position, g_World);
	oPosH = mul(oPosH, g_View);
	oPosH = mul(oPosH, g_Proj);

	return (oPosH);
}