//
//  (c) 2015 James Hannam
//
//  File Name   :   unlit.vs
//  Description :   A basic unlit vertex shader.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#include "unlit.sh"

// Vertex Shader
PSIn
VS(VSIn input)
{
	PSIn output;
	
	output.posH = LocalToClipSpace(input.posL);

	return output;
}