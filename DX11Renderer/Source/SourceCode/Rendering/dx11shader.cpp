//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11shader.cpp
//  Description :   An abstract DirectX 11 shader.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <D3Dcommon.h>
#include <D3D11.h>
#include <D3Dcompiler.h>
#include <cassert>

// Local Includes
#include "utility.h"

// This Include
#include "dx11shader.h"

// Static Variables

// Static Function Prototypes4

// Implementation
Bool
DX11Shader::Initialise(/*ID3D11Device* _In_ _pDevice,*/
					   ComPtr<ID3D11Device> _pDevice,
					   /*ID3D11DeviceContext* _In_ _pDeviceContext,*/
					   ComPtr<ID3D11DeviceContext> _pDeviceContext,
					   HWND _In_ _pWindow)
{
	assert(_pDevice != nullptr);
	assert(_pDeviceContext != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pDeviceContext != nullptr)
		{
			//m_pDeviceContext = _pDeviceContext;
			//m_pDeviceContext->AddRef();

			m_pDeviceContext = _pDeviceContext;

			bSucceeded = InitialiseShaderData(_pDevice, _pWindow);

			if(bSucceeded)
			{
				bSucceeded = false;

				if(m_pVertexShader != nullptr)
				{
					if(m_pPixelShader != nullptr)
					{
						if(m_pLayout != nullptr)
						{
							bSucceeded = true;
						}
					}
				}
			}
		}
	}

	return (bSucceeded);
}

// Protected Functions
DX11Shader::DX11Shader()
: /*m_pDeviceContext(nullptr)
, */m_pVertexShader(nullptr)
, m_pGeometryShader(nullptr)
, m_pPixelShader(nullptr)
, m_pLayout(nullptr)
, m_ePrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED)
{
}

DX11Shader::~DX11Shader()
{
	Shutdown();
}

void
DX11Shader::Shutdown()
{
	ReleaseCOMNull(m_pLayout);
	ReleaseCOMNull(m_pPixelShader);
	ReleaseCOMNull(m_pGeometryShader);
	ReleaseCOMNull(m_pVertexShader);
	//ReleaseCOMNull(m_pDeviceContext);
}

Bool
DX11Shader::InitialiseInputLayout(/*ID3D11Device* _In_ _pDevice,*/
								  ComPtr<ID3D11Device> _pDevice,
								  const D3D11_INPUT_ELEMENT_DESC* _In_ _pLayout,
								  const UInt32 _iNumLayoutElements,
								  ID3DBlob* _In_ _pVertexShaderBuffer)
{
	assert(_pDevice != nullptr);
	assert(_pLayout != nullptr);
	assert(_pVertexShaderBuffer != nullptr);

	Bool bSucceeded = false;
	
	if(_pDevice != nullptr)
	{
		if(_pLayout != nullptr)
		{
			if(_pVertexShaderBuffer != nullptr)
			{
				HRESULT iResult = _pDevice->CreateInputLayout(_pLayout,
															  _iNumLayoutElements,
															  _pVertexShaderBuffer->GetBufferPointer(),
															  _pVertexShaderBuffer->GetBufferSize(),
															  &m_pLayout);

				if(SUCCEEDED(iResult))
				{
					bSucceeded = true;
				}
			}
			else
			{
				// Null vertex shader buffer error!
				// TODO: Create an error logger.
			}
		}
		else
		{
			// Null layout error!
		}
	}
	else
	{
		// Null device error!
	}

	return (bSucceeded);
}

Bool
DX11Shader::CompileShader(const EShaderType _eShaderType,
						  const wchar_t* const _In_ _psFilename,
						  const Int8* const _In_ _psFunctionName,
						  HWND _In_ _pWindow,
						  ID3DBlob** _Out_ _ppVertexShaderBuffer)
{
	assert(_psFilename != nullptr);
	assert(_psFunctionName != nullptr);
	assert(_ppVertexShaderBuffer != nullptr);
	assert((*_ppVertexShaderBuffer) == nullptr);

	Bool bSucceeded = false;

	if(_psFilename != nullptr)
	{
		if(_psFunctionName != nullptr)
		{
			if(_ppVertexShaderBuffer != nullptr)
			{
				if((*_ppVertexShaderBuffer) == nullptr)
				{
					Int8* psShaderProfile = nullptr;

					switch(_eShaderType)
					{
						case(SHADERTYPE_VERTEX):
						{
							psShaderProfile = "vs_5_0";
							break;
						}
						case(SHADERTYPE_GEOMETRY):
						{
							psShaderProfile = "gs_5_0";
							break;
						}
						case(SHADERTYPE_PIXEL):
						{
							psShaderProfile = "ps_5_0";
							break;
						}
					}

					if(psShaderProfile != nullptr)
					{
						ID3DBlob* pErrorMessage = nullptr;

						UINT iCompileFlags = D3D10_SHADER_ENABLE_STRICTNESS;

						#if defined(DEBUG) || defined(_DEBUG)
							iCompileFlags |= D3D10_SHADER_DEBUG;
						#endif //defined(DEBUG) || defined(_DEBUG)

						// TODO: Ensure that this method of passing out the shader buffer works as intended.
							HRESULT iResult = D3DCompileFromFile(_psFilename,
																 nullptr,
																 D3D_COMPILE_STANDARD_FILE_INCLUDE,
																 _psFunctionName,
																 psShaderProfile,
																 iCompileFlags,
																 0,
																 _ppVertexShaderBuffer,
																 (&pErrorMessage));
						if(SUCCEEDED(iResult))
						{
							bSucceeded = true;
						}
						else
						{
							OutputShaderErrorMessage(pErrorMessage, _pWindow, _psFilename);
						}

		
						ReleaseCOMNull(pErrorMessage);
					}
					else
					{
						// Invalid shader type error!
						assert(false);
					}
				}
			}
		}
	}
	
	return (bSucceeded);
}

Bool
DX11Shader::InitialiseVertexShader(ComPtr<ID3D11Device> _pDevice,
								   ID3DBlob* _In_ _pVertexShaderBuffer,
								   ID3D11ClassLinkage* _In_ _pClassLinkage)
{
	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pVertexShaderBuffer != nullptr)
		{
			HRESULT iResult = _pDevice->CreateVertexShader(_pVertexShaderBuffer->GetBufferPointer(),
														   _pVertexShaderBuffer->GetBufferSize(),
														   _pClassLinkage,
														   &m_pVertexShader);

			bSucceeded = SUCCEEDED(iResult);
		}
	}	

	return (bSucceeded);
}

Bool
DX11Shader::InitialiseGeometryShader(ComPtr<ID3D11Device> _pDevice,
									 ID3DBlob* _In_ _pGeometryShaderBuffer,
									 ID3D11ClassLinkage* _In_ _pClassLinkage)
{
	assert(_pDevice != nullptr);
	assert(_pGeometryShaderBuffer != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pGeometryShaderBuffer != nullptr)
		{
			HRESULT iResult = _pDevice->CreateGeometryShader(_pGeometryShaderBuffer->GetBufferPointer(),
															 _pGeometryShaderBuffer->GetBufferSize(),
															 _pClassLinkage,
															 &m_pGeometryShader);

			bSucceeded = SUCCEEDED(iResult);
		}
	}


	return (bSucceeded);
}
		
Bool
DX11Shader::InitialisePixelShader(ComPtr<ID3D11Device> _pDevice,
								  ID3DBlob* _In_ _pPixelShaderBuffer,
								  ID3D11ClassLinkage* _In_ _pClassLinkage)
{
	assert(_pDevice != nullptr);
	assert(_pPixelShaderBuffer != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pPixelShaderBuffer != nullptr)
		{
			HRESULT iResult = _pDevice->CreatePixelShader(_pPixelShaderBuffer->GetBufferPointer(),
														  _pPixelShaderBuffer->GetBufferSize(),
														  _pClassLinkage,
														  &m_pPixelShader);

			bSucceeded = SUCCEEDED(iResult);
		}
	}

	return (bSucceeded);
}

void
DX11Shader::SetShaders()
{
	assert(m_pDeviceContext != nullptr);
	assert(m_pVertexShader != nullptr);
	assert(m_pPixelShader != nullptr);

	if(m_pDeviceContext != nullptr)
	{
		if(m_pVertexShader != nullptr)
		{
			if(m_pPixelShader != nullptr)
			{
				// Set the primitive type that should be used for rendering this shader.
				m_pDeviceContext->IASetPrimitiveTopology(m_ePrimitiveTopology);

				// Set the vertex input layout.
				m_pDeviceContext->IASetInputLayout(m_pLayout);

				// Set the vertex, geometry and pixel shaders to be used for drawing.
				m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
				m_pDeviceContext->GSSetShader(m_pGeometryShader, nullptr, 0);
				m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
			}
		}
	}
}

void
DX11Shader::OutputShaderErrorMessage(ID3DBlob* _In_ _pErrorMessage,
									 HWND _In_ _pWindow,
									 const wchar_t* const _In_ _psShaderFilename)
{
	assert(_psShaderFilename != nullptr);

	if(_psShaderFilename != nullptr)
	{
		if(_pErrorMessage != nullptr)
		{
			wchar_t* psCompileErrors = nullptr;
			UInt32 iErrorLength = 0;

			// Get a pointer to the error message text buffer.
			Int8* psTempCompileErrors = reinterpret_cast<Int8*>(_pErrorMessage->GetBufferPointer());

			// Get the length of the message.
			iErrorLength = _pErrorMessage->GetBufferSize();

			psCompileErrors = new wchar_t[iErrorLength];

			// Convert the error to a wide string.
			size_t iConverted = 0;
			mbstowcs_s(&iConverted, psCompileErrors, iErrorLength, psTempCompileErrors, iErrorLength);

			psTempCompileErrors = nullptr;
	
			// Output the error string to the output window.
			OutputDebugString(L"\nShader Compilation Error:\n");
			OutputDebugString(psCompileErrors);
			OutputDebugString(L"\n");

			DeleteArrayNull(psCompileErrors);

			// Display a message box to notify the user to
				// check the outout window for compiler errors.
			MessageBox(_pWindow,
					   L"Error compiling shader. Check the output window for details.",
					   _psShaderFilename,
					   MB_OK);
		}
		else
		{
			MessageBox(_pWindow, _psShaderFilename, L"Missing Shader File!", MB_OK);
		}
	}
}