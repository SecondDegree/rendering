//
//  (c) 2014-15 James Hannam
//
//  File Name   :   colour.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes

// Local Includes

// This Include
#include "colour.h"

// Static Variables

// Static Function Prototypes

// Implementation
Colour::Colour()
{
	(*this) = Colour::Black();
}

Colour::Colour(const Float32 _rRed,
			   const Float32 _rGreen,
			   const Float32 _rBlue,
			   const Float32 _rAlpha)
{
	Set(_rRed,
		_rGreen,
		_rBlue,
		_rAlpha);
}

Colour::Colour(const UInt8 _iRed,
			   const UInt8 _iGreen,
			   const UInt8 _iBlue,
			   const UInt8 _iAlpha)
{
	Set(_iRed,
		_iGreen,
		_iBlue,
		_iAlpha);
}

Colour::~Colour()
{
}

// Accessors
// Set
void
Colour::Set(const Float32 _rRed,
			const Float32 _rGreen,
			const Float32 _rBlue,
			const Float32 _rAlpha)
{
	m_rRed = _rRed;
	m_rGreen = _rGreen;
	m_rBlue = _rBlue;
	m_rAlpha = _rAlpha;
}

void
Colour::Set(const UInt8 _iRed,
			const UInt8 _iGreen,
			const UInt8 _iBlue,
			const UInt8 _iAlpha)
{
	m_rRed = static_cast<Float32>(_iRed) / 255.0f;
	m_rGreen = static_cast<Float32>(_iGreen) / 255.0f;
	m_rBlue = static_cast<Float32>(_iBlue) / 255.0f;
	m_rAlpha = static_cast<Float32>(_iAlpha) / 255.0f;
}

// Operators
void
Colour::operator =(const Colour& _RHS)
{
	m_rRed = _RHS.m_rRed;
	m_rGreen = _RHS.m_rGreen;
	m_rBlue = _RHS.m_rBlue;
	m_rAlpha = _RHS.m_rAlpha;
}

// Static Colours
Colour
Colour::Black()
{
	return (Colour(0.0f, 0.0f, 0.0f, 1.0f));
}

Colour
Colour::White()
{
	return (Colour(1.0f, 1.0f, 1.0f, 1.0f));
}

Colour
Colour::Grey()
{
	return (Colour(0.5f, 0.5f, 0.5f, 1.0f));
}

Colour
Colour::Red()
{
	return (Colour(1.0f, 0.0f, 0.0f, 1.0f));
}

Colour
Colour::Green()
{
	return (Colour(0.0f, 1.0f, 0.0f, 1.0f));
}

Colour
Colour::Blue()
{
	return (Colour(0.0f, 0.0f, 1.0f, 1.0f));
}

Colour
Colour::Magenta()
{
	return (Colour(1.0f, 0.0f, 1.0f, 1.0f));
}

Colour
Colour::Yellow()
{
	return (Colour(1.0f, 1.0f, 0.0f, 1.0f));
}

Colour
Colour::Cyan()
{
	return (Colour(0.0f, 1.0f, 1.0f, 1.0f));
}

Colour
Colour::Orange()
{
	return (Colour(1.0f, 0.5f, 0.0f, 1.0f));
}

Colour
Colour::Salmon()
{
	return (Colour(0.98f, 0.5f, 0.447f, 1.0f));
}