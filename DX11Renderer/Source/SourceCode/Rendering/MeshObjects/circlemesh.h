//
//  (c) 2015 James Hannam
//
//  File Name   :   circlemesh.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Preprocessor Directives

// Library Includes

// Local Includes
#include "dx11unlitmesh.h"

// Types

// Constants

// Prototypes

MESH_SUBCLASS_BASIC(CircleMesh)