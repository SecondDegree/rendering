//
//  (c) 2015 James Hannam
//
//  File Name   :   circlemesh.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Preprocessor Directives

// Library Includes
#include <math.h>

// Local Includes
#include "utility.h"
#include "mathutility.h"

// This Include
#include "circlemesh.h"

// Static Variables

// Prototypes

// Implementation
Bool
CircleMesh::InitialiseMeshData(ComPtr<ID3D11Device> _pDevice)
{
	assert(_pDevice != nullptr);

	Bool bSucceeded = false;
	Bool bVertexBufferInitialised = false;
	Bool bIndexBufferInitialised = false;

	if(_pDevice != nullptr)
	{
		const UInt32 iNumOuterPoints = 24; // Circle fidelity
		const Float32 fRadius = 0.5f;
		const UInt32 iNumVertices = iNumOuterPoints + 1; // One more for the centre point.

		Vertex pVertices[iNumVertices];

		pVertices[0] = {{0.0f, 0.0f, 0.0f}};

		// Start at 1, 0 is the centre point.
		for(UInt8 i = 1; i < iNumVertices; ++i)
		{
			// The angle of this point - determined by a ratio of the total number of points
			Float32 fAngle = (static_cast<float>(i + 1) / static_cast<Float32>(iNumOuterPoints)) * MathUtility::Pi::TwoPi;
							//				X							Y				  Z
			pVertices[i] = {Vector3((fRadius * sinf(fAngle)), -(fRadius * cosf(fAngle)), 0.0f)};
		}

		DX11VertexBufferDesc vertexBufferDesc;
		vertexBufferDesc.m_pVertices = pVertices;
		vertexBufferDesc.m_iNumVetices = iNumVertices;
		vertexBufferDesc.m_iVertexStride = sizeof(pVertices[0]);

		bVertexBufferInitialised = InitialiseVertexBuffer(_pDevice, vertexBufferDesc);

		if(bVertexBufferInitialised)
		{
			// Create the index array.
			const UInt32 iNumIndices = iNumOuterPoints * 3;

			UInt32 piIndices[iNumIndices];
			UInt8 iIndex = 0;

			for(UInt8 i = 1; i < iNumVertices; ++i)
			{
								// Wrap back to 1 if we reach then end.
				UInt8 iNext = ((i + 1) == iNumVertices) ? 1 : (i + 1);

				piIndices[iIndex++] = 0; // Centre
				piIndices[iIndex++] = iNext; // Clockwise winding
				piIndices[iIndex++] = i;
			}

			assert(iIndex == iNumIndices);

			DX11IndexBufferDesc indexBufferDesc;
			indexBufferDesc.m_piIndices = piIndices;
			indexBufferDesc.m_iNumIndices = iNumIndices;

			bIndexBufferInitialised = InitialiseIndexBuffer(_pDevice, indexBufferDesc);

			if(bIndexBufferInitialised)
			{
				bSucceeded = true;
			}
		}
	}

	return (bSucceeded);
}