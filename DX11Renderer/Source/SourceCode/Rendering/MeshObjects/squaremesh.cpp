//
//  (c) 2015 James Hannam
//
//  File Name   :   squaremesh.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Preprocessor Directives

// Library Includes

// Local Includes
#include "utility.h"

// This Include
#include "squaremesh.h"

// Static Variables

// Prototypes

// Implementation
Bool
SquareMesh::InitialiseMeshData(ComPtr<ID3D11Device> _pDevice)
{
	assert(_pDevice != nullptr);

	Bool bSucceeded = false;
	Bool bVertexBufferInitialised = false;
	Bool bIndexBufferInitialised = false;

	if(_pDevice != nullptr)
	{
		Vertex pVertices[] = {
								{{-0.5f, -0.5f, 0.0f}},
								{{-0.5f,  0.5f, 0.0f}},
								{{ 0.5f,  0.5f, 0.0f}},
								{{ 0.5f, -0.5f, 0.0f}},
							 };

		const UInt32 iNumVertices = ArraySize(pVertices);

		DX11VertexBufferDesc vertexBufferDesc;
		vertexBufferDesc.m_pVertices = pVertices;
		vertexBufferDesc.m_iNumVetices = iNumVertices;
		vertexBufferDesc.m_iVertexStride = sizeof(pVertices[0]);

		bVertexBufferInitialised = InitialiseVertexBuffer(_pDevice, vertexBufferDesc);

		if(bVertexBufferInitialised)
		{

			// Create the index array.
			UInt32 piIndices[] = {
									// Top-left
									0,
									1,
									2,

									// Bottom-right
									0,
									2,
									3,
								 };

			const UInt32 iNumIndices = ArraySize(piIndices);

			DX11IndexBufferDesc indexBufferDesc;
			indexBufferDesc.m_piIndices = piIndices;
			indexBufferDesc.m_iNumIndices = iNumIndices;

			bIndexBufferInitialised = InitialiseIndexBuffer(_pDevice, indexBufferDesc);

			if(bIndexBufferInitialised)
			{
				bSucceeded = true;
			}
		}
	}

	return (bSucceeded);
}