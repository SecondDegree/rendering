//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11texture.h
//  Description :   A basic wrapper for a DirectX 11 texture.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <wrl.h>

// Local Includes
#include "types.h"

// Types
using Microsoft::WRL::ComPtr;

// Constants

// Prototypes
struct ID3D11Device;
struct ID3D11ShaderResourceView;

class DX11Texture
{
    // Member Functions
	public:
		DX11Texture();
		~DX11Texture();

		Bool LoadTextureFromFile(ComPtr<ID3D11Device> _pDevice,
								 const wchar_t* const _In_ _psFilename);
		void ReleaseTexture();

		// Accessors
		// Get
		ID3D11ShaderResourceView* GetTexture() const;

	private:
		DX11Texture(const DX11Texture&);
		DX11Texture operator =(const DX11Texture&);

    // Member Variables
	private:
		ID3D11ShaderResourceView* m_pTexture;
};