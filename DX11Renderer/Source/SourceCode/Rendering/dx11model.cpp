//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11model.cpp
//  Description :   An abstract DirectX 11 3D model.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <D3D11.h>
#include <cassert>

// Local Includes
#include "utility.h"
#include "dx11mesh.h"

// This Include
#include "dx11model.h"

// Static Variables

// Static Function Prototypes

// Implementation

// Protected Functions.
DX11Model::DX11Model(void)
{
}

DX11Model::~DX11Model(void)
{
	Shutdown();
}

Bool
DX11Model::Initialise(ComPtr<ID3D11Device> _pDevice,
					  ComPtr<ID3D11DeviceContext> _pDeviceContext,
					  std::shared_ptr<DX11Mesh> _pMesh)
{
	assert(_pDevice != nullptr);
	assert(_pDeviceContext != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pDeviceContext != nullptr)
		{
			m_pDeviceContext = _pDeviceContext;

			if(_pMesh != nullptr)
			{
				m_pMesh = _pMesh;
				bSucceeded = true;
			}
			else
			{
				// TODO: Get the mesh from the mesh factory!
				// TODO: Should be making a textured mesh!!
				//m_pMesh = std::make_shared<DX11Mesh>();

				//if(m_pMesh != nullptr)
				//{
				//	bSucceeded = m_pMesh->Initialise(_pDeviceContext);
				//}
				//else
				//{
				//	assert(false); // Out of memory?
				//}
			}

			if(bSucceeded)
			{
				// Initialise the vertex and index buffer that hold the geometry.
				bSucceeded = InitialiseModelData(_pDevice);
			}
		}
	}

	return (bSucceeded);
}

void
DX11Model::Draw(void)
{
	if(m_pMesh != nullptr)
	{
		m_pMesh->SetBuffers();

		m_pMesh->Draw();
	}
}

void
DX11Model::SetMesh(std::shared_ptr<DX11Mesh> _pNewMesh)
{
	m_pMesh = _pNewMesh;
}

void
DX11Model::Shutdown(void)
{

}