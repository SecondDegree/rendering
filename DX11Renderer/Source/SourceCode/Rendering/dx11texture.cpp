//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11texture.cpp
//  Description :   A basic wrapper for a DirectX 11 texture.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <D3D11.h>
#include <DDSTextureLoader.h>
#include <WICTextureLoader.h>
#include <cassert>

// Local Includes
#include "utility.h"

// This Include
#include "dx11texture.h"

using DirectX::CreateDDSTextureFromFile;
using DirectX::CreateWICTextureFromFile;

// Static Variables

// Static Function Prototypes

// Implementation
DX11Texture::DX11Texture()
: m_pTexture(nullptr)
{
}

DX11Texture::~DX11Texture()
{
	ReleaseTexture();
}

Bool
DX11Texture::LoadTextureFromFile(/*ID3D11Device* _In_ _pDevice,*/
ComPtr<ID3D11Device> _pDevice,
								 const wchar_t* const _In_ _psFilename)
{
	assert(_pDevice != nullptr);
	assert(_psFilename != nullptr);

	Bool bSucceeded = false;

	HRESULT iResult = -1;
	
	// Release the currently held texture (if there is one).
	ReleaseTexture();

	// Load the texture from disk.
	// TODO: Add support for loading from all common image types (especially .tga)
	// TODO: Add support for storing the returned ID3DResource.
	iResult = CreateWICTextureFromFile(_pDevice.Get(), _psFilename, nullptr, &m_pTexture);

	if(SUCCEEDED(iResult))
	{
		bSucceeded = true;
	}

	return (bSucceeded);
}

void
DX11Texture::ReleaseTexture()
{
	ReleaseCOMNull(m_pTexture);
}

// Accessors
// Get
ID3D11ShaderResourceView*
DX11Texture::GetTexture() const
{
	return m_pTexture;
}