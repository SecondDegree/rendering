//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11modeltextured.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <cassert>
#include <D3D11.h>

// Local Includes
#include "utility.h"
#include "dx11texture.h"
#include "dx11mesh.h"

// This Include
#include "dx11modeltextured.h"

// Static Variables

// Static Function Prototypes

// Implementation
DX11ModelTextured::DX11ModelTextured()
: m_pTexture(nullptr)
{

}

DX11ModelTextured::~DX11ModelTextured()
{
	DeleteNull(m_pTexture);
}

void
DX11ModelTextured::Shutdown()
{
	DX11Model::Shutdown();
}

Bool
DX11ModelTextured::LoadTexture(ComPtr<ID3D11Device> _pDevice,
							   const wchar_t* const _psFilename)
{
	Bool bSucceeded = false;

	if(m_pTexture == nullptr)
	{
		m_pTexture = new DX11Texture;
	}
	
	bSucceeded = m_pTexture->LoadTextureFromFile(_pDevice, _psFilename);

	return (bSucceeded);
}

// Accessors
// Get
ID3D11ShaderResourceView*
DX11ModelTextured::GetTexture() const
{
	return (m_pTexture->GetTexture());
}

// Protected Functions
Bool
DX11ModelTextured::InitialiseModelData(ComPtr<ID3D11Device> _pDevice)
{
	assert(_pDevice != nullptr);
	assert(m_pMesh != nullptr);

	if(m_pMesh == nullptr) return (false);

	Bool bSucceeded = false;
	Bool bVertexBufferInitialised = false;
	Bool bIndexBufferInitialised = false;

	if(_pDevice != nullptr)
	{
		const UInt32 iNumVertices = 3;

		Vertex pVertices[iNumVertices];

		pVertices[0].m_Position = Vector3(-1.0f, -1.0f, 0.0f);
		pVertices[1].m_Position = Vector3(0.0f, 1.0f, 0.0f);
		pVertices[2].m_Position = Vector3(1.0f, -1.0f, 0.0f);

		pVertices[0].m_UVCoord = Vector2(0.0f, 1.0f);
		pVertices[1].m_UVCoord = Vector2(0.5f, 0.0f);
		pVertices[2].m_UVCoord = Vector2(1.0f, 1.0f);

		DX11VertexBufferDesc vertexBufferDesc;
		vertexBufferDesc.m_pVertices = pVertices;
		vertexBufferDesc.m_iNumVetices = iNumVertices;
		vertexBufferDesc.m_iVertexStride = sizeof(pVertices[0]);

		bVertexBufferInitialised = m_pMesh->InitialiseVertexBuffer(_pDevice, vertexBufferDesc);

		if(bVertexBufferInitialised)
		{
			const UInt32 iNumIndices = 3;

			// Create the index array.
			UInt32 piIndices[iNumIndices];

			piIndices[0] = 0;
			piIndices[1] = 1;
			piIndices[2] = 2;

			DX11IndexBufferDesc indexBufferDesc;
			indexBufferDesc.m_piIndices = piIndices;
			indexBufferDesc.m_iNumIndices = iNumIndices;

			bIndexBufferInitialised = m_pMesh->InitialiseIndexBuffer(_pDevice, indexBufferDesc);

			if(bIndexBufferInitialised)
			{
				bSucceeded = true;
			}
		}
	}

	return (bSucceeded);
}