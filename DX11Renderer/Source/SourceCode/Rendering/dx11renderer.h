//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11renderer.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <Windows.h>
#include <DXGI.h>
#include <sal.h>
#include <SimpleMath.h>
#include <d3d11.h>
#include <wrl.h>

// Local Includes
#include "types.h"
#include "colour.h"
#include "utility.h"

// Preprocessor Directives
PragmaLib("dxgi")
PragmaLib("d3d11")

PragmaLib_d("directxtk")

// Types
using DirectX::SimpleMath::Matrix;
using Microsoft::WRL::ComPtr;

// Constants

// Prototypes
class Window;

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11Texture2D;
struct ID3D11DepthStencilState;
struct ID3D11DepthStencilView;
struct ID3D11RasterizerState;

class DX11Renderer
{
    // Member Functions
	public:
		DX11Renderer(void);
		~DX11Renderer(void);

		Bool Initialise(const Window& _Window, const Colour& _ClearColour);
		void Shutdown(void);
		
		void BeginRender(void);
		void EndRender(void);

		Bool ResizeBuffers(UInt32 _iNewWidth, UInt32 _iNewHeight);

		// Debug

		// Accessors
		// Get
		ComPtr<ID3D11Device> GetDevice(void) const;
		ComPtr<ID3D11DeviceContext> GetDeviceContext(void) const;
		
		ComPtr<ID3D11Debug> GetDebug(void) const;

		// Set
		void SetClearColour(const Colour& _Colour);

	private:
		DX11Renderer(const DX11Renderer&) = delete;
		DX11Renderer operator =(const DX11Renderer&) = delete;

		/*
		* The returned refresh rate will be 0/1 if VSync is enabled.
		*/
		Bool _GetAdapterInfo(const Int32 _iScreenWidth,
							 const Int32 _iScreenHeight,
							 DXGI_RATIONAL& _Out_ _RefreshRate);
		Bool _GetRefreshRate(const Int32 _iScreenWidth,
							 const Int32 _iScreenHeight,
							 DXGI_RATIONAL& _Out_ _RefreshRate,
							 IDXGIAdapter* const _In_ _pAdapter);

		Bool _GetSwapChainBuffer(UINT _iBufferIndex, ID3D11Texture2D*& _Out_ _pBufferTexture);

		Bool _CreateRenderTargetView(void);
		Bool _CreateDepthStencilView(void);
		
		Bool _InitialiseDeviceAndSwapChain(const Int32 _iScreenWidth,
										   const Int32 _iScreenHeight,
										   const Bool _bIsFullscreen,
										   const DXGI_RATIONAL& _RefreshRate,
										   HWND _pWindow);

		Bool _InitialiseDepthBuffer(const Int32 _iScreenWidth,
									const Int32 _iScreenHeight);
		Bool _InitialiseDepthStencil(void);

    // Member Variables
	private:
		enum
		{
			DESCRIPTION_LENGTH = 128
		};

		Bool m_bVSyncEnabled;
		Float32 m_rNearPlane;
		Float32 m_rFarPlane;
		Int32 m_iVideoCardMemory;
		Int8 m_iVideoCardDescription[DESCRIPTION_LENGTH];

		Colour m_ClearColour;
		
		// DirectX Types
		IDXGISwapChain* m_pSwapChain;

		ComPtr<ID3D11Device> m_pDevice;
		ComPtr<ID3D11DeviceContext> m_pDeviceContext;

		ID3D11RenderTargetView* m_pRenderTargetView;

		ID3D11Texture2D* m_pDepthStencilBuffer;
		ID3D11DepthStencilState* m_pDepthStencilState;
		ID3D11DepthStencilView* m_pDepthStencilView;
		D3D11_DEPTH_STENCIL_VIEW_DESC m_DepthStencilViewDesc;

		ID3D11RasterizerState* m_pRasterizerState;

		//ID3D11Debug* m_pDebug;
		ComPtr<ID3D11Debug> m_pDebug;
};