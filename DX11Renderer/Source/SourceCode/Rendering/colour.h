//
//  (c) 2014-15 James Hannam
//
//  File Name   :   colour.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes

// Local Includes
#include "types.h"

// Types

// Constants

// Prototypes

// TODO: Consider inheriting from DirectX::SimpleMath::Color; has useful additional functionality.
class Colour
{
    // Member Functions
	public:
		Colour();
		Colour(const Float32 _rRed,
			   const Float32 _rGreen,
			   const Float32 _rBlue,
			   const Float32 _rAlpha);
		Colour(const UInt8 _iRed,
			   const UInt8 _iGreen,
			   const UInt8 _iBlue,
			   const UInt8 _iAlpha);
		// TODO: Copy constructor.
		~Colour();

		// Accessors
		// Set
		void Set(const Float32 _rRed,
			     const Float32 _rGreen,
			     const Float32 _rBlue,
			     const Float32 _rAlpha);
		void Set(const UInt8 _iRed,
				 const UInt8 _iGreen,
				 const UInt8 _iBlue,
				 const UInt8 _iAlpha);

		// Operators
		void operator =(const Colour& _RHS);

		// Static Colours
		static Colour Black();
		static Colour White();
		static Colour Grey();

		static Colour Red();
		static Colour Green();
		static Colour Blue();

		static Colour Magenta();
		static Colour Yellow();
		static Colour Cyan();

		static Colour Orange();

		static Colour Salmon();

    // Member Variables
	public:
		// We use a union here to save space, this will
			// use only 16 bytes of data instead of 32
		union
		{
			struct
			{
				Float32 m_rRed;
				Float32 m_rGreen;
				Float32 m_rBlue;
				Float32 m_rAlpha;
			};

			Float32 m_rColour[4];
		};
};