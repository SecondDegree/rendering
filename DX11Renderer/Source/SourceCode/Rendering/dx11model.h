//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11model.h
//  Description :   An abstract DirectX 11 3D model.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <Windows.h>
#include <sal.h>
#include <memory>
#include <wrl.h>

// Local Includes
#include "types.h"

// Types
using Microsoft::WRL::ComPtr;

// Constants

// Prototypes
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Buffer;
enum D3D11_USAGE;
class DX11Mesh;

class DX11Model abstract
{
    // Member Functions
	protected:
		DX11Model();
		virtual ~DX11Model();

	public:		
		Bool Initialise(ComPtr<ID3D11Device> _pDevice,
						ComPtr<ID3D11DeviceContext> _pDeviceContext,
						std::shared_ptr<DX11Mesh> _pMesh = nullptr);
		void Draw();

		void SetMesh(std::shared_ptr<DX11Mesh> _pNewMesh);

	protected:

		void Shutdown();

		// TODO: Redesign this to work like the space invaders implementation.

		// Pure Virtual Functions
		/*
		* Called by Initialise.
		* The vertex buffer must be initialised inside.
		*/
		virtual Bool InitialiseModelData(ComPtr<ID3D11Device> _pDevice) abstract;

    // Member Variables
	protected:
		ComPtr<ID3D11DeviceContext> m_pDeviceContext;

		std::shared_ptr<DX11Mesh> m_pMesh;

		// TODO: Consider redesigning this to be component based.
};
