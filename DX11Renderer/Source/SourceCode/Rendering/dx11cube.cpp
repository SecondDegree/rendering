//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11cube.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <cassert>

// Local Includes

// This Include
#include "dx11cube.h"

// Static Variables

// Static Function Prototypes

// Implementation
DX11Cube::DX11Cube()
{
}

DX11Cube::~DX11Cube()
{
}

Bool
DX11Cube::InitialiseModelData(ID3D11Device* _In_ _pDevice)
{
	assert(_pDevice != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		const UInt32 iNumVertices = 8;

		Vertex* pVertices = new Vertex[iNumVertices];

		for(UInt32 iVertex = 0; iVertex < iNumVertices; ++iVertex)
		{
			pVertices[iVertex].m_Colour = Colour::Yellow();
		}

		// TODO: This is not a cube .__.
		pVertices[0].m_Position = Vector3(0, 0, 0);
		pVertices[1].m_Position = Vector3(0, 0, 0);
		pVertices[2].m_Position = Vector3(0, 0, 0);
		pVertices[3].m_Position = Vector3(0, 0, 0);
		pVertices[4].m_Position = Vector3(0, 0, 0);
		pVertices[5].m_Position = Vector3(0, 0, 0);
		pVertices[6].m_Position = Vector3(0, 0, 0);
		pVertices[7].m_Position = Vector3(0, 0, 0);
	}

	return (bSucceeded);
}