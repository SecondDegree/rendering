//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dxcamera.h
//  Description :   A DirectX Camera.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <SimpleMath.h>

// Local Includes
#include "types.h"
#include "dxobject.h"

using DirectX::SimpleMath::Matrix;
using DirectX::SimpleMath::Vector3;

// Types

// Constants
enum class ProjectionMode : Int8
{
	Orthographic = 0,
	Perspective
};

// Prototypes

class DXCamera : public DXObject
{
    // Member Functions
	public:
		DXCamera(void);
		virtual ~DXCamera(void);

		/*
		* Field of view in degrees.
		*/
		Bool Initialise(Float32 _fFieldOfView,
						Float32 _fViewportWidth,
						Float32 _fViewportHeight,
						Float32 _fNearPlane,
						Float32 _fFarPlane,
						ProjectionMode _ProjectionMode = ProjectionMode::Perspective);

		virtual void Process(Float32 _fDeltaTick) override;

		// Accessors
		// Get
		// Matrix
		const Matrix& GetView(void) const;
		const Matrix& GetProjection(void) const;

		// Other
		/*
		* Returns field of view in degrees.
		*/
		Float32 GetFieldOfView(void) const;

		Float32 GetViewportWidth(void) const;
		Float32 GetViewportHeight(void) const;

		Float32 GetNearPlane(void) const;
		Float32 GetFarPlane(void) const;

		ProjectionMode GetMode(void) const;

		// Set
		/*
		* Field of view in degrees.
		* Hard clamped to 5-175 degrees.
		*/
		void SetFieldOfView(Float32 _fNewFieldOfView);

		void SetViewportWidth(Float32 _fNewViewportWidth);
		void SetViewportHeight(Float32 _fNewViewportHeight);

		void SetNearPlane(Float32 _fNewNearPlane);
		void SetFarPlane(Float32 _fNewFarPlane);
		
		void SetMode(ProjectionMode _NewProjectionMode);

		void AddFieldOfView(Float32 _fFieldOfViewOffset);

		void AddViewportWidth(Float32 _fViewportWidthOffset);
		void AddViewportHeight(Float32 _fViewportHeightOffset);

		void AddNearPlane(Float32 _fNearPlaneOffset);
		void AddFarPlane(Float32 _fFarPlaneOffset);
		
	private:
		DXCamera(const DXCamera&) = delete;
		DXCamera operator =(const DXCamera&) = delete;

		void RecalculateView(void);
		void RecalculateProjection(void);

		virtual void OnRecalculateWorld(void) override;

    // Member Variables
	private:
		Matrix m_View;
		Matrix m_Projection;

		Bool m_bRecalculateProjection;

		ProjectionMode m_ProjectionMode;

		/*
		* Field of view stored in radians.
		*/
		Float32 m_fFieldOfView;

		Float32 m_fViewportWidth;
		Float32 m_fViewportHeight;

		Float32 m_fNearPlane;
		Float32 m_fFarPlane;

		// TODO: Add a viewport pointer once they are implemented.
};