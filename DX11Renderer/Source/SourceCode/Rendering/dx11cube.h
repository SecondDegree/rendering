//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11cube.h
//  Description :   A 3D cube model.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <SimpleMath.h>
#include <sal.h>

// Local Includes
#include "types.h"
#include "dx11model.h"
#include "colour.h"

using DirectX::SimpleMath::Vector3;

// Types

// Constants

// Prototypes

class DX11Cube : public DX11Model
{
	// Internal Types
	private:
		struct Vertex
		{
			Vector3 m_Position;
			Colour m_Colour; // Move this to be a shader input.
		};

    // Member Functions
	public:
		DX11Cube();
		virtual ~DX11Cube();

	private:
		DX11Cube(const DX11Cube&) = delete;
		DX11Cube operator =(const DX11Cube&) = delete;

		virtual Bool InitialiseModelData(ID3D11Device* _In_ _pDevice);

    // Member Variables
	private:

};