//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11mesh.h
//  Description :   A basic static mesh of vertices and indices
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <wrl.h>

// Local Includes
#include "types.h"
#include "dx11meshdefines.h"

// Types

// Constants

// Prototypes
using Microsoft::WRL::ComPtr;

class DX11Mesh abstract
{
	// Member Functions
	protected:
		DX11Mesh(void);

	public:
		virtual ~DX11Mesh(void);

		/*
		* Stores the device context for later use.
		*/
		Bool Initialise(ComPtr<ID3D11Device> _pDevice,
						ComPtr<ID3D11DeviceContext> _pDeviceContext);

		void Shutdown(void);

		// NOTE: This is currently a completely static mesh class.
		Bool InitialiseVertexBuffer(ComPtr<ID3D11Device> _pDevice,
								    const DX11VertexBufferDesc& _VertexBufferDesc);

		Bool InitialiseIndexBuffer(ComPtr<ID3D11Device> _pDevice,
								   const DX11IndexBufferDesc& _IndexBufferDesc);

		/*
		* To be called before Draw/DrawIndexed.
		* Sets the buffers on the card to be passed into shaders.
		*/
		void SetBuffers(void);

		void Draw(void);
		
		// Operators
		Bool operator==(const DX11Mesh& _Other) const;

	protected:
		virtual Bool InitialiseMeshData(ComPtr<ID3D11Device> _pDevice) abstract;

    // Member Variables
	private:
		ComPtr<ID3D11DeviceContext> m_pDeviceContext;

		ID3D11Buffer* m_pVertexBuffer;
		ID3D11Buffer* m_pIndexBuffer;
		UInt32 m_iNumVertices;
		UInt32 m_iNumIndices;
		UInt32 m_iVertexStride;
};