//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11shader.h
//  Description :   An abstract DirectX 11 shader.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Precompiler Directives
#pragma comment(lib, "d3dcompiler.lib")

// Library Includes
#include <d3dcommon.h>
#include <Windows.h>
#include <sal.h>
#include <wrl.h>

// Local Includes
#include "types.h"

// Types

// Constants

// Prototypes
using Microsoft::WRL::ComPtr;

struct ID3D11VertexShader;
struct ID3D11GeometryShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11ClassLinkage;
struct D3D11_INPUT_ELEMENT_DESC;
enum D3D_PRIMITIVE_TOPOLOGY;

// TODO: Create helper macros for inheriting new shader subclasses; currently over complicated and repetitive.

class DX11Shader
{
	// Internal Types
	protected:
		enum EShaderType // TODO: Convert enums to enum classes
		{
			INVALID_SHADERTYPE = -1,
			SHADERTYPE_VERTEX,
			SHADERTYPE_GEOMETRY,
			SHADERTYPE_PIXEL,
			//SHADERTYPE_DOMAIN,
			//SHADERTYPE_HULL,
			//SHADERTYPE_TESSELLATION,
			MAX_SHADERTYPE
		};

    // Member Functions
	public:
		Bool Initialise(/*ID3D11Device* _In_ _pDevice,*/
						ComPtr<ID3D11Device> _pDevice,
						/*ID3D11DeviceContext* _In_ _pDeviceContext,*/
						ComPtr<ID3D11DeviceContext> _pDeviceContext,
						HWND _In_ _pWindow); // TODO: Remove the window from this call chain.

	protected:
		// Can only be created and destroyed by children.
		DX11Shader();
		virtual ~DX11Shader();
		
		void Shutdown();

		// TODO: Redesign this to work like the space invaders implementation.
		
		Bool InitialiseInputLayout(/*ID3D11Device* _In_ _pDevice,*/
								   ComPtr<ID3D11Device> _pDevice,
								   const D3D11_INPUT_ELEMENT_DESC* _In_ _pLayout,
								   const UInt32 _iNumLayoutElements,
								   ID3DBlob* _In_ _pVertexShaderBuffer);

		// TODO: Add support for compiling out to binary for later loading.
		// TODO: Add support for run-time recompiling; useful for tweaking.

		// Shader Compilation Functions
		// Compiles straight from shader files (.vs, .ps, etc..)
		Bool CompileShader(const EShaderType _eShaderType,
						   const wchar_t* const _In_ _psFilename,
						   const Int8* const _In_ _psFunctionName,
						   HWND _In_ _pWindow,
						   ID3DBlob** _Out_ _ppVertexShaderBuffer);

		// Shader Initialisation Functions
		// TODO: Add an InitialiseShaders function which initialises all shader types.
		// TODO: Add a MakeShader (or something of the like) function to the renderer to avoid passing around the device so much.
		//			Or maybe even have a shader factory - just make it more concise.
		Bool InitialiseVertexShader(ComPtr<ID3D11Device> _pDevice,
									ID3DBlob* _In_ _pVertexShaderBuffer,
									ID3D11ClassLinkage* _In_ _pClassLinkage = nullptr);
		
		Bool InitialiseGeometryShader(ComPtr<ID3D11Device> _pDevice,
									  ID3DBlob* _In_ _pGeometryShaderBuffer,
									  ID3D11ClassLinkage* _In_ _pClassLinkage = nullptr);
		
		Bool InitialisePixelShader(ComPtr<ID3D11Device> _pDevice,
								   ID3DBlob* _In_ _pPixelShaderBuffer,
								   ID3D11ClassLinkage* _In_ _pClassLinkage = nullptr);
		
		// TODO: Write a templatised map function for constant buffers.

		/*
		* Sets all shaders (as well as the input layout and primitive topology)
		*	onto the graphics card to be used for any following draw calls.
		*/
		virtual void SetShaders();

		void OutputShaderErrorMessage(ID3DBlob* _In_ _pErrorMessage,
									  HWND _In_ _pWindow,
									  const wchar_t* const _In_ _psShaderFilename);

		// Pure virtual Functions.
		/*
		* Called by Initialise.
		* Vertex and pixel shaders, input layout and primitive topology
		*	must be initialised inside.
		*/
		virtual Bool InitialiseShaderData(ComPtr<ID3D11Device> _pDevice,
										  HWND _In_ _pWindow) abstract;
		
    // Member Variables
	protected:
		//ID3D11DeviceContext* m_pDeviceContext;
		ComPtr<ID3D11DeviceContext> m_pDeviceContext;

		ID3D11VertexShader* m_pVertexShader;
		ID3D11GeometryShader* m_pGeometryShader;
		ID3D11PixelShader* m_pPixelShader;
		
		ID3D11InputLayout* m_pLayout;

		D3D_PRIMITIVE_TOPOLOGY m_ePrimitiveTopology;
};