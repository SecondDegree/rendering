//
//  (c) 2013-15 James Hannam
//
//  File Name   :   dx11renderer.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <D3D11.h>
#include <D3Dcommon.h>
#include <DirectXMath.h>
#include <cassert>

// Local Includes
#include "utility.h"
#include "window.h"
#include "dx11meshfactory.h"

// This Include
#include "dx11renderer.h"

using DirectX::XMConvertToRadians;

// Static Variables

// Static Function Prototypes

// Implementation
DX11Renderer::DX11Renderer(void)
: m_bVSyncEnabled(true)
, m_rNearPlane(0.1f)
, m_rFarPlane(500.0f)
, m_iVideoCardMemory(0)
, m_ClearColour(Colour::Grey())
// DirectX Types
, m_pSwapChain(nullptr)
, m_pDevice(nullptr)
, m_pDeviceContext(nullptr)
, m_pRenderTargetView(nullptr)
, m_pDepthStencilBuffer(nullptr)
, m_pDepthStencilState(nullptr)
, m_pDepthStencilView(nullptr)
, m_pRasterizerState(nullptr)
{
	SecureZeroMemory(m_iVideoCardDescription, DESCRIPTION_LENGTH);
	SecureZeroMemory(&m_DepthStencilViewDesc, sizeof(m_DepthStencilViewDesc));
}

DX11Renderer::~DX11Renderer(void)
{
	Shutdown();
}

Bool
DX11Renderer::Initialise(const Window& _Window, const Colour& _ClearColour)
{
	Bool bSucceeded = false;

	HRESULT iResult = 0;
	const Int32 iWindowWidth = _Window.GetClientWidth();
	const Int32 iWindowHeight = _Window.GetClientHeight();
	const Float32 fWindowWidth = _Window.GetClientWidthF();
	const Float32 fWindowHeight = _Window.GetClientHeightF();
	D3D_FEATURE_LEVEL eFeatureLevel = D3D_FEATURE_LEVEL_11_1;
	DXGI_RATIONAL refreshRate = {0, 1};

	ID3D11Texture2D* pBackBuffer = nullptr;

	D3D11_RASTERIZER_DESC rasterizerDesc;
	D3D11_VIEWPORT viewport;


	m_ClearColour = _ClearColour;

	bSucceeded = _GetAdapterInfo(iWindowWidth, iWindowHeight, refreshRate);
	if(bSucceeded)
	{
		bSucceeded = _InitialiseDeviceAndSwapChain(iWindowWidth,
												  iWindowHeight,
												  _Window.GetIsFullscreen(),
												  refreshRate,
												  _Window.GetHandle());
		if(bSucceeded)
		{
			// Set succeeded back to false as it will
				// be set to true if everything succeeds.
			bSucceeded = false;

			if(_CreateRenderTargetView())
			{
				// TODO: Split this mess up into logical functions.

				// Additional information on depth stencil:
					// https://msdn.microsoft.com/en-us/library/windows/desktop/bb205074(v=vs.85).aspx
				D3D11_TEXTURE2D_DESC depthBufferDesc;
				SecureZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

				// Set up the description of the depth buffer.
				depthBufferDesc.Width = iWindowWidth;
				depthBufferDesc.Height = iWindowHeight;
				depthBufferDesc.MipLevels = 1;
				depthBufferDesc.ArraySize = 1;
				depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
				depthBufferDesc.SampleDesc.Count = 1;
				depthBufferDesc.SampleDesc.Quality = 0;
				depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
				depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
				depthBufferDesc.CPUAccessFlags = 0;
				depthBufferDesc.MiscFlags = 0;

				// Create the texture for the depth buffer.
				iResult = m_pDevice->CreateTexture2D(&depthBufferDesc, NULL, &m_pDepthStencilBuffer);
				if(SUCCEEDED(iResult))
				{
					D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
					SecureZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

					// Set up the description of the stencil state.
					depthStencilDesc.DepthEnable = true;
					depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
					depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

					depthStencilDesc.StencilEnable = true;
					depthStencilDesc.StencilReadMask = 0xFF;
					depthStencilDesc.StencilWriteMask = 0xFF;

					// Stencil operations if pixel is front-facing.
					depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
					depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
					depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
					depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

					// Stencil operations if pixel is back-facing.
					depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
					depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
					depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
					depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

					// Create the depth stencil state.
					iResult = m_pDevice->CreateDepthStencilState(&depthStencilDesc, &m_pDepthStencilState);
					if(SUCCEEDED(iResult))
					{
						// Set the depth stencil state.
						m_pDeviceContext->OMSetDepthStencilState(m_pDepthStencilState, 1);

						// Set up the depth stencil view description.
						m_DepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
						m_DepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
						m_DepthStencilViewDesc.Texture2D.MipSlice = 0;

						// Create the depth stencil view.
						if(_CreateDepthStencilView())
						{
							// Bind the render target view and depth stencil
								// buffer to the output render pipeline.
							m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);

							// Setup the raster description which will determine
								// how and what polygons will be drawn.
							rasterizerDesc.FillMode = D3D11_FILL_SOLID;
							rasterizerDesc.CullMode = D3D11_CULL_BACK;
							rasterizerDesc.DepthBias = 0;
							rasterizerDesc.SlopeScaledDepthBias = 0.0f;
							rasterizerDesc.DepthBiasClamp = 0.0f;
							rasterizerDesc.DepthClipEnable = true;
							rasterizerDesc.FrontCounterClockwise = false;
							rasterizerDesc.ScissorEnable = false;
							rasterizerDesc.MultisampleEnable = false;
							rasterizerDesc.AntialiasedLineEnable = true;

							// Create the rasterizer state.
							iResult = m_pDevice->CreateRasterizerState(&rasterizerDesc, &m_pRasterizerState);
							if(SUCCEEDED(iResult))
							{
								// Now set the rasterizer state.
								m_pDeviceContext->RSSetState(m_pRasterizerState);

								// Setup the viewport for rendering.
								viewport.Width = fWindowWidth;
								viewport.Height = fWindowHeight;
								viewport.MinDepth = 0.0f;
								viewport.MaxDepth = 1.0f;
								viewport.TopLeftX = 0.0f;
								viewport.TopLeftY = 0.0f;

								// Set the viewport.
								m_pDeviceContext->RSSetViewports(1, &viewport);

								if(DX11MeshFactory::Initialise(m_pDevice, m_pDeviceContext))
								{
									bSucceeded = true;
								} // END DX11MeshFactory Initialise
							} // END CreateRasterizerState
						} // END CreateDepthStencilView
					} // END CreateDepthStencilState
				} // END CreateTexture2D
			} // END CreateRenderTargetView
		} // END InitialiseDeviceAndSwapChain
	}
	
	return (bSucceeded);
}

void
DX11Renderer::Shutdown(void)
{
	DX11MeshFactory::Shutdown();

	// Before shutting down, set to windowed mode or when
		// we release the swap chain it will throw an exception.
	if(m_pSwapChain != nullptr)
	{
		m_pSwapChain->SetFullscreenState(false, NULL);
	}

	ReleaseCOMNull(m_pRasterizerState);
	ReleaseCOMNull(m_pDepthStencilView);
	ReleaseCOMNull(m_pDepthStencilState);
	ReleaseCOMNull(m_pDepthStencilBuffer);
	ReleaseCOMNull(m_pRenderTargetView);

	ReleaseCOMNull(m_pSwapChain);

#if defined(_DEBUG)
	if(m_pDebug != nullptr)
	{
		//m_pDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	}
#endif
}

void
DX11Renderer::BeginRender(void)
{
	// Clear the back buffer.
	m_pDeviceContext->ClearRenderTargetView(m_pRenderTargetView, m_ClearColour.m_rColour);

	// Clear the depth buffer.
	m_pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void
DX11Renderer::EndRender(void)
{
	// Present the back buffer to the screen.
	m_pSwapChain->Present((m_bVSyncEnabled == true), 0);
}

Bool
DX11Renderer::ResizeBuffers(UInt32 _iNewWidth, UInt32 _iNewHeight)
{
	Bool bSucceeded = false;

	if(m_pSwapChain != nullptr)
	{
		// TODO: Test that this function works as intended.

		// Remove the current render targets (render target view and depth stencil view).
		m_pDeviceContext->OMSetRenderTargets(0, nullptr, nullptr);

		// Release the render target view
		ReleaseCOMNull(m_pRenderTargetView);

		// Release the depth stencil view
		ReleaseCOMNull(m_pDepthStencilView);

		// Resize all buffers
		HRESULT iResult = m_pSwapChain->ResizeBuffers(0, _iNewWidth, _iNewHeight, DXGI_FORMAT_UNKNOWN, 0);

		if(SUCCEEDED(iResult))
		{
			// Recreate render target views
			if(_CreateRenderTargetView())
			{
				if(_CreateDepthStencilView())
				{
					m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);

					bSucceeded = true;
				}
			}
		}
	}

	return (bSucceeded);
}

// Accessors
// Get
ComPtr<ID3D11Device>
DX11Renderer::GetDevice(void) const
{
	return (m_pDevice);
}

ComPtr<ID3D11DeviceContext>
DX11Renderer::GetDeviceContext(void) const
{
	return (m_pDeviceContext);
}

ComPtr<ID3D11Debug>
DX11Renderer::GetDebug(void) const
{
	return (m_pDebug);
}

//Set
void
DX11Renderer::SetClearColour(const Colour& _Colour)
{
	m_ClearColour = _Colour;
}

// Private Functions
Bool
DX11Renderer::_GetAdapterInfo(const Int32 _iScreenWidth,
							  const Int32 _iScreenHeight,
							  DXGI_RATIONAL& _Out_ _RefreshRate)
{
	Bool bSucceeded = false;

	HRESULT iResult = 0;
	UInt32 iStringLength = 0;
	IDXGIFactory* pFactory = nullptr;
	IDXGIAdapter* pAdapter = nullptr;
	DXGI_ADAPTER_DESC adapterDesc;

	// Create a DirectX graphics interface factory.
	iResult = CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&pFactory));
	if(SUCCEEDED(iResult))
	{
		// Use the factory to create an adapter for the primary graphics interface.
		iResult = pFactory->EnumAdapters(0, &pAdapter);
		if(SUCCEEDED(iResult))
		{
			if(m_bVSyncEnabled)
			{
				bSucceeded = _GetRefreshRate(_iScreenWidth, _iScreenHeight, _RefreshRate, pAdapter);
			}
			else
			{
				_RefreshRate.Numerator = 0;
				_RefreshRate.Denominator = 1;
				bSucceeded = true;
			}

			if(bSucceeded)
			{
				bSucceeded = false;

				// Get the adapter description.
				iResult = pAdapter->GetDesc(&adapterDesc);
				if(SUCCEEDED(iResult))
				{
					// Store the dedicated video card memory in megabytes.
					m_iVideoCardMemory = static_cast<Int32>(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

					// Convert the name of the video card to a character array and store it.
					Int32 iError = wcstombs_s(NULL,
											  m_iVideoCardDescription,
											  DESCRIPTION_LENGTH,
											  adapterDesc.Description,
											  DESCRIPTION_LENGTH);
					if(iError == 0)
					{
						bSucceeded = true;
					}
				} // END GetDesc
			}

			ReleaseCOMNull(pAdapter);
		} // END EnumAdapters

		if(pFactory != nullptr)
		{
			pFactory->Release();
			pFactory = nullptr;
		}
	} // END CreateDXGIFactory

	return (bSucceeded);
}

Bool
DX11Renderer::_GetRefreshRate(const Int32 _iScreenWidth,
							  const Int32 _iScreenHeight,
							  DXGI_RATIONAL& _Out_ _RefreshRate,
							  IDXGIAdapter* const _In_ _pAdapter)
{
	Bool bSucceeded = false;
	
	UInt32 iNumModes = 0;
	IDXGIOutput* pAdapterOutput = nullptr;
	DXGI_MODE_DESC* pDisplayModes = nullptr;

	// Enumerate the primary adapter output (monitor).
	HRESULT iResult = _pAdapter->EnumOutputs(0, &pAdapterOutput);
	if(SUCCEEDED(iResult))
	{
		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format.
		iResult = pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM,
														DXGI_ENUM_MODES_INTERLACED,
														&iNumModes,
														NULL);
		if(SUCCEEDED(iResult))
		{
			// Create a list to hold all the possible display
			// modes for this monitor/video card combination.
			pDisplayModes = new DXGI_MODE_DESC[iNumModes];
			if(pDisplayModes != nullptr)
			{
				// Populate the list with display modes.
				iResult = pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM,
																DXGI_ENUM_MODES_INTERLACED,
																&iNumModes,
																pDisplayModes);
				if(SUCCEEDED(iResult))
				{
					UInt32 iWidth = static_cast<UInt32>(_iScreenWidth);
					UInt32 iHeight = static_cast<UInt32>(_iScreenHeight);

					// Iterate through all of the display modes and find the one that
						// matches the screen width and height and get it's refresh rate.
					for(UInt32 iIndex = 0; iIndex < iNumModes; ++iIndex)
					{
						const DXGI_MODE_DESC& displayMode = pDisplayModes[iIndex];

						if(displayMode.Width == iWidth)
						{
							if(displayMode.Height == iHeight)
							{
								_RefreshRate = displayMode.RefreshRate;
								bSucceeded = true;
							}
						}
					}
				} // END Adapter GetDisplayModeList
				
				DeleteArrayNull(pDisplayModes);
			} // END new DXGI_MODE_DESC

														
		} // END Output GetDisplayModeList
					
		ReleaseCOMNull(pAdapterOutput);
	} // END EnumOutputs

	return (bSucceeded);
}

Bool 
DX11Renderer::_GetSwapChainBuffer(UINT _iBufferIndex, ID3D11Texture2D*& _Out_ _pBufferTexture)
{
	Bool bSucceeded = false;

	if(m_pSwapChain != nullptr)
	{
		ID3D11Texture2D* pBuffer = nullptr;
		HRESULT iResult = m_pSwapChain->GetBuffer(_iBufferIndex, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBuffer));

		if(SUCCEEDED(iResult))
		{
			_pBufferTexture = pBuffer;
			bSucceeded = true;
		}
		else
		{
			// TODO: Error handling for get buffer failure.
		}
	}

	return (bSucceeded);
}

Bool
DX11Renderer::_CreateRenderTargetView(void)
{
	Bool bSucceeded = false;

	if(m_pDevice != nullptr)
	{
		ReleaseCOMNull(m_pRenderTargetView);

		ID3D11Texture2D* pBackBuffer = nullptr;

		if(_GetSwapChainBuffer(0, pBackBuffer))
		{
			HRESULT iResult = m_pDevice->CreateRenderTargetView(pBackBuffer, nullptr, &m_pRenderTargetView);

			if(SUCCEEDED(iResult))
			{
				bSucceeded = true;
			}

			pBackBuffer->Release();
			pBackBuffer = nullptr;
		}
	}

	return (bSucceeded);
}

Bool
DX11Renderer::_CreateDepthStencilView(void)
{
	Bool bSucceeded = false;

	if(m_pDevice != nullptr && m_pDepthStencilBuffer != nullptr)
	{
		ReleaseCOMNull(m_pDepthStencilView);

		HRESULT iResult = m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer,
															&m_DepthStencilViewDesc,
															&m_pDepthStencilView);

		if(SUCCEEDED(iResult))
		{
			bSucceeded = true;
		}
	}

	return (bSucceeded);
}

Bool
DX11Renderer::_InitialiseDeviceAndSwapChain(const Int32 _iScreenWidth,
										    const Int32 _iScreenHeight,
										    const Bool _bIsFullscreen,
										    const DXGI_RATIONAL& _RefreshRate,
										    HWND _pWindow)
{
	Bool bSucceeded = false;
	HRESULT iResult = 0;
	
	D3D_FEATURE_LEVEL eFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	
	SecureZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	// Set to a single back buffer.
	swapChainDesc.BufferCount = 1;

	// Set the width and height of the back buffer.
	swapChainDesc.BufferDesc.Width = _iScreenWidth;
	swapChainDesc.BufferDesc.Height = _iScreenHeight;

	// Set regular 32-bit surface for the back buffer.
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	// Set the refresh rate of the back buffer.
	swapChainDesc.BufferDesc.RefreshRate = _RefreshRate;

	// Set the usage of the back buffer.
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	// Set the handle for the window to render to.
	swapChainDesc.OutputWindow = _pWindow;

	// Turn multi sampling off.
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;

	// Set to full screen or windowed mode.
	swapChainDesc.Windowed = (_bIsFullscreen == false);

	// Set the scan line ordering and scaling to unspecified.
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	// Discard the back buffer contents after presenting.
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Don't set any advanced flags.
	swapChainDesc.Flags = 0;

	UINT iCreationFlags = 0;

	#if defined(_DEBUG)
		iCreationFlags |= D3D11_CREATE_DEVICE_DEBUG;
	#endif // defined(_DEBUG)

	ID3D11Device* pDevice = nullptr;
	ID3D11DeviceContext* pDeviceContext = nullptr;

	// Create the swap chain, Direct3D device and Direct3D device context.
	iResult = D3D11CreateDeviceAndSwapChain(nullptr,
											D3D_DRIVER_TYPE_HARDWARE,
											NULL,
											iCreationFlags, // Flags
											&eFeatureLevel,
											1, // Feature level count
											D3D11_SDK_VERSION,
											&swapChainDesc,
											&m_pSwapChain,
											&pDevice,
											NULL,
											&pDeviceContext);
	if(SUCCEEDED(iResult))
	{
		m_pDevice = pDevice;
		pDevice->Release();
		pDevice = nullptr;

		m_pDeviceContext = pDeviceContext;
		pDeviceContext->Release();
		pDeviceContext = nullptr;

#if defined(_DEBUG)
		ID3D11Debug* pDebug = nullptr;
		m_pDevice.Get()->QueryInterface(&pDebug);
		if(pDebug != nullptr)
		{
			m_pDebug = pDebug;
			pDebug->Release();
			pDebug = nullptr;
		}
#endif // defined(_DEBUG)

		bSucceeded = true;
	}
	else
	{
		const wchar_t* const piMessage = L"Failed to create DX11 Device and Swap Chain!\n"
										 L"Please ensure that the default graphics card has DirectX 11 support.";
		MessageBox(_pWindow, piMessage, L"Error!", MB_OK);
	}

	return (bSucceeded);
}