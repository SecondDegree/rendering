//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11meshdefines.h
//  Description :   Contains definitions for data structures/enumerations used
//						by the DX11Mesh and DX11MeshFactory classes.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <D3D11.h>

// Local Includes
#include "types.h"

// Prototypes

// Types
struct DX11BufferDesc
{
	D3D11_USAGE m_eUsage;
	UInt32 m_iCPUAccessFlags;
	UInt32 m_iMiscFlags;

	DX11BufferDesc()
	: m_eUsage(D3D11_USAGE_DEFAULT)
	, m_iCPUAccessFlags(0)
	, m_iMiscFlags(0)
	{
	}
};

// TODO: Add functions to convert these to D3D11_BUFFER_DESC and D3D11_SUBRESOURCE_DATA.

struct DX11VertexBufferDesc : public DX11BufferDesc
{
	// TODO: Change this to a template class.
	UInt32 m_iNumVetices;
	UInt32 m_iVertexStride; // sizeof the vertex type.
	const void* m_pVertices;

	DX11VertexBufferDesc()
	: m_iNumVetices(0)
	, m_iVertexStride(0)
	, m_pVertices(nullptr)
	{
	}
};

struct DX11IndexBufferDesc : public DX11BufferDesc
{
	UInt32 m_iNumIndices;
	const UInt32* m_piIndices;

	DX11IndexBufferDesc()
	: m_iNumIndices(0)
	, m_piIndices(nullptr)
	{
	}
};

// Something like...
template<class _T>
struct DX11BufferDescX : public DX11BufferDesc
{
	UInt32 m_iNumData;
	const _T* m_pData;

	DX11BufferDescX()
	: m_iNumData(0)
	, m_pData(nullptr)
	{}
};

// Then make the specific descs something like...
template<class _VertexType>
struct DX11VertexBufferDescX : DX11BufferDescX<_VertexType>
{};

using DX11IndexBufferDescX = DX11BufferDescX<UInt32>;

// Constants

// Macros
