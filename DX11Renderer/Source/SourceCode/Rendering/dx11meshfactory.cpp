//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11meshfactory.cpp
//  Description :   A singleton which creates and stores DX11Mesh objects, helps to avoid
//						duplication of mesh data.
//						NOTE: Dynamic mesh data will (currently) have to be handled externally.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <cassert>
#include <memory>

// Local Includes
#include "utility.h"
#include "dx11mesh.h"

// This Include
#include "dx11meshfactory.h"

// Static Variables

// Static Function Prototypes

// Implementation

// Private Functions
DX11MeshFactory::DX11MeshFactory()
{
	/*for(UInt32 iMesh = 0; iMesh < MAX_MESH; ++iMesh)
	{
		m_pMeshes[iMesh] = nullptr;
	}*/
}

DX11MeshFactory::~DX11MeshFactory()
{
	/*for(UInt32 iMesh = 0; iMesh < MAX_MESH; ++iMesh)
	{
		DeleteNull(m_pMeshes[iMesh]);
	}*/
}

// Public Functions
DX11MeshFactory&
DX11MeshFactory::GetInstance()
{
	static std::unique_ptr<DX11MeshFactory> s_pInstance(new DX11MeshFactory);

	return (*(s_pInstance));
}

Bool
DX11MeshFactory::Initialise(ComPtr<ID3D11Device> _pDevice,
							ComPtr<ID3D11DeviceContext> _pDeviceContext)
{
	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pDeviceContext != nullptr)
		{
			auto& instance = GetInstance();

			instance.m_pDevice = _pDevice;

			instance.m_pDeviceContext = _pDeviceContext;

			bSucceeded = true;
		}
	}

	return (bSucceeded);
}

void
DX11MeshFactory::Shutdown(void)
{
}