//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dxcamera.cpp
//  Description :   A DirectX Camera.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <DirectXMath.h>
#include <cassert>

// Local Includes
#include "mathutility.h"

// This Include
#include "dxcamera.h"

// Static Variables

// Static Function Prototypes

// Implementation
DXCamera::DXCamera(void)
: m_ProjectionMode(ProjectionMode::Perspective)
, m_bRecalculateProjection(false)
, m_fFieldOfView(DirectX::XMConvertToRadians(70.0f))
, m_fViewportWidth(4.0f)
, m_fViewportHeight(3.0f)
, m_fNearPlane(0.01f)
, m_fFarPlane(100.0f)
{
	RecalculateView();
}

DXCamera::~DXCamera(void)
{
}

Bool
DXCamera::Initialise(Float32 _fFieldOfView,
					 Float32 _fViewportWidth,						 
					 Float32 _fViewportHeight,						 
					 Float32 _fNearPlane,						  
					 Float32 _fFarPlane,
					 ProjectionMode _ProjectionMode)
{
	Bool bSucceeded = false;

	SetFieldOfView(_fFieldOfView);
	SetViewportWidth(_fViewportWidth);
	SetViewportHeight(_fViewportHeight);
	SetNearPlane(_fNearPlane);
	SetFarPlane(_fFarPlane);

	SetMode(_ProjectionMode);

	RecalculateProjection();

	bSucceeded = true;

	return (bSucceeded);
}

void
DXCamera::Process(Float32 _fDeltaTick)
{
	DXObject::Process(_fDeltaTick);

	if(m_bRecalculateProjection)
	{
		RecalculateProjection();
	}
}

// Accessors
// Get
const Matrix&
DXCamera::GetView(void) const
{
	return (m_View);
}

const Matrix&
DXCamera::GetProjection(void) const
{
	return (m_Projection);
}

// Other
Float32
DXCamera::GetFieldOfView(void) const
{
	return (DirectX::XMConvertToDegrees(m_fFieldOfView));
}

Float32
DXCamera::GetViewportWidth(void) const
{
	return (m_fViewportWidth);
}

Float32
DXCamera::GetViewportHeight(void) const
{
	return (m_fViewportHeight);
}

Float32
DXCamera::GetNearPlane(void) const
{
	return (m_fNearPlane);
}

Float32
DXCamera::GetFarPlane(void) const
{
	return (m_fFarPlane);
}

ProjectionMode
DXCamera::GetMode(void) const
{
	return (m_ProjectionMode);
}

// Set
void
DXCamera::SetFieldOfView(Float32 _fNewFieldOfView)
{
	m_fFieldOfView = DirectX::XMConvertToRadians(MathUtility::Clamp(_fNewFieldOfView, 5.0f, 175.0f));
	m_bRecalculateProjection = true;
}

void
DXCamera::SetViewportWidth(Float32 _fNewViewportWidth)
{
	m_fViewportWidth = _fNewViewportWidth;
	m_bRecalculateProjection = true;
}

void
DXCamera::SetViewportHeight(Float32 _fNewViewportHeight)
{
	m_fViewportHeight = _fNewViewportHeight;
	m_bRecalculateProjection = true;
}

void
DXCamera::SetNearPlane(Float32 _fNewNearPlane)
{
	m_fNearPlane = _fNewNearPlane;
	m_bRecalculateProjection = true;
}

void
DXCamera::SetFarPlane(Float32 _fNewFarPlane)
{
	m_fFarPlane = _fNewFarPlane;
	m_bRecalculateProjection = true;
}

void
DXCamera::SetMode(ProjectionMode _NewProjectionMode)
{
	m_ProjectionMode = _NewProjectionMode;
	m_bRecalculateProjection = true;
}

void
DXCamera::AddFieldOfView(Float32 _fFieldOfViewOffset)
{
	SetFieldOfView(GetFieldOfView() + _fFieldOfViewOffset);
}

void
DXCamera::AddViewportWidth(Float32 _fViewportWidthOffset)
{
	SetViewportWidth(GetViewportWidth() + _fViewportWidthOffset);
}

void
DXCamera::AddViewportHeight(Float32 _fViewportHeightOffset)
{
	SetViewportHeight(GetViewportHeight() + _fViewportHeightOffset);
}

void
DXCamera::AddNearPlane(Float32 _fNearPlaneOffset)
{
	SetNearPlane(GetNearPlane() + _fNearPlaneOffset);
}

void
DXCamera::AddFarPlane(Float32 _fFarPlaneOffset)
{
	SetFarPlane(GetFarPlane() + _fFarPlaneOffset);
}


// Private Functions
void
DXCamera::RecalculateView(void)
{
	m_View = Matrix::CreateLookAt(GetPos(), (GetPos() + GetWorld().Forward()), GetWorld().Up());
}

void
DXCamera::RecalculateProjection(void)
{
	switch(m_ProjectionMode)
	{
		case (ProjectionMode::Perspective):
		{
			m_Projection = Matrix::CreatePerspectiveFieldOfView(m_fFieldOfView,
																(m_fViewportWidth / m_fViewportHeight),
																m_fNearPlane,
																m_fFarPlane);
			break;
		}
		case (ProjectionMode::Orthographic):
		{
			m_Projection = Matrix::CreateOrthographic(m_fViewportWidth, m_fViewportHeight, m_fNearPlane, m_fFarPlane);
			break;
		}
		default:
		{
			// Something broke! (unsupported projection mode [somehow...]).
			assert(false);
			break;
		}
	}

	m_bRecalculateProjection = false;
}

void
DXCamera::OnRecalculateWorld(void)
{
	RecalculateView();
}