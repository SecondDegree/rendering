//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11mesh.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <D3D11.h>
#include <cassert>

// Local Includes
#include "utility.h"

// This Include
#include "dx11mesh.h"

// Static Variables

// Static Function Prototypes

// Implementation
DX11Mesh::DX11Mesh()
: m_pDeviceContext(nullptr)
, m_pVertexBuffer(nullptr)
, m_pIndexBuffer(nullptr)
, m_iNumVertices(0)
, m_iNumIndices(0)
, m_iVertexStride(0)
{
}

DX11Mesh::~DX11Mesh()
{
	Shutdown();
}

Bool
DX11Mesh::Initialise(ComPtr<ID3D11Device> _pDevice,
					 ComPtr<ID3D11DeviceContext> _pDeviceContext)
{
	assert(_pDeviceContext != nullptr);

	Bool bSucceeded = false;

	if(_pDeviceContext != nullptr)
	{
		m_pDeviceContext = _pDeviceContext;

		bSucceeded = InitialiseMeshData(_pDevice);
	}

	return (bSucceeded);
}

void
DX11Mesh::Shutdown()
{
	// Release the vertex and index buffers.
	ReleaseCOMNull(m_pIndexBuffer);
	ReleaseCOMNull(m_pVertexBuffer);
}

Bool
DX11Mesh::InitialiseVertexBuffer(ComPtr<ID3D11Device> _pDevice,
								 const DX11VertexBufferDesc& _VertexBufferDesc)
{
	assert(_pDevice != nullptr);
	assert(_VertexBufferDesc.m_pVertices != nullptr);
	assert(_VertexBufferDesc.m_iNumVetices > 0);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_VertexBufferDesc.m_pVertices != nullptr)
		{
			if(_VertexBufferDesc.m_iNumVetices > 0)
			{
				D3D11_BUFFER_DESC vertexDesc;
				D3D11_SUBRESOURCE_DATA vertexData;

				m_iNumVertices = _VertexBufferDesc.m_iNumVetices;
				m_iVertexStride = _VertexBufferDesc.m_iVertexStride;

				// Fill out the description of the vertex buffer.
				vertexDesc.Usage = _VertexBufferDesc.m_eUsage;
				vertexDesc.ByteWidth = m_iVertexStride * m_iNumVertices;
				vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
				vertexDesc.CPUAccessFlags = _VertexBufferDesc.m_iCPUAccessFlags;
				vertexDesc.MiscFlags = _VertexBufferDesc.m_iMiscFlags;
				vertexDesc.StructureByteStride = 0;

				// Fill out the subresource data of the vertex buffer.
				vertexData.pSysMem = _VertexBufferDesc.m_pVertices;
				vertexData.SysMemPitch = 0;
				vertexData.SysMemSlicePitch = 0;

				// Create the vertex buffer.
				HRESULT iResult = _pDevice->CreateBuffer(&vertexDesc, &vertexData, &m_pVertexBuffer);
				if(SUCCEEDED(iResult))
				{
					bSucceeded = true;
				}
			}
		}
	}

	return (bSucceeded);
}

Bool
DX11Mesh::InitialiseIndexBuffer(ComPtr<ID3D11Device> _pDevice,
								const DX11IndexBufferDesc& _IndexBufferDesc)
{
	assert(_pDevice != nullptr);
	assert(_IndexBufferDesc.m_piIndices != nullptr);
	assert(_IndexBufferDesc.m_iNumIndices > 0);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_IndexBufferDesc.m_piIndices != nullptr)
		{
			if(_IndexBufferDesc.m_iNumIndices > 0)
			{
				D3D11_BUFFER_DESC indexDesc;
				D3D11_SUBRESOURCE_DATA indexData;

				m_iNumIndices = _IndexBufferDesc.m_iNumIndices;

				// Fill out the description of the index buffer.
				indexDesc.Usage = _IndexBufferDesc.m_eUsage;
				indexDesc.ByteWidth = sizeof(_IndexBufferDesc.m_piIndices[0]) * m_iNumIndices;
				indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
				indexDesc.CPUAccessFlags = _IndexBufferDesc.m_iCPUAccessFlags;
				indexDesc.MiscFlags = _IndexBufferDesc.m_iMiscFlags;
				indexDesc.StructureByteStride = 0;

				// Fill out the subresource data of the index buffer.
				indexData.pSysMem = _IndexBufferDesc.m_piIndices;
				indexData.SysMemPitch = 0;
				indexData.SysMemSlicePitch = 0;

				HRESULT iResult = _pDevice->CreateBuffer(&indexDesc, &indexData, &m_pIndexBuffer);
				if(SUCCEEDED(iResult))
				{
					bSucceeded = true;
				}
			}
		}
	}

	return (bSucceeded);
}

void
DX11Mesh::SetBuffers()
{
	assert(m_pDeviceContext != nullptr);

	if(m_pDeviceContext != nullptr)
	{
		if(m_pVertexBuffer != nullptr)
		{
			UInt32 iOffset = 0;

			// TODO: Add support for subsetted buffers.

			// Set the vertex buffer to active in the input assembler for rendering.
			m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_iVertexStride, &iOffset);

			if(m_pIndexBuffer != nullptr)
			{
				// Set the index buffer to active in the input assembler for rendering.
				m_pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
			}
		}
	}
}

void
DX11Mesh::Draw(void)
{
	assert(m_pDeviceContext != nullptr);

	if(m_pDeviceContext != nullptr)
	{
		// Draw the mesh.
		if(m_pIndexBuffer != nullptr)
		{
			m_pDeviceContext->DrawIndexed(m_iNumIndices, 0, 0);
		}
		else
		{
			m_pDeviceContext->Draw(m_iNumVertices, 0);
		}
	}
}

Bool
DX11Mesh::operator==(const DX11Mesh& _Other) const
{
	return ((m_pVertexBuffer == _Other.m_pVertexBuffer) &&
			(m_pIndexBuffer == _Other.m_pIndexBuffer));
}