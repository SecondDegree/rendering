//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11meshfactory.h
//  Description :   A singleton which creates and stores DX11Mesh objects, helps to avoid
//						duplication of mesh data.
//						NOTE: Does not support dynamic meshes (yet).
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <sal.h>
#include <memory>
#include <wrl.h>

// Local Includes
#include "types.h"

// Types
using Microsoft::WRL::ComPtr;

// Constants

// Prototypes
class DX11Mesh;
struct ID3D11Device;
struct ID3D11DeviceContext;

class DX11MeshFactory
{
	friend std::default_delete<DX11MeshFactory>;

    // Member Functions
	private:
		DX11MeshFactory();
		~DX11MeshFactory();

		DX11MeshFactory(const DX11MeshFactory&) = delete;
		void operator =(const DX11MeshFactory&) = delete;

	public:
		static DX11MeshFactory& GetInstance();

		// TODO: Unfinished - Add logic to build and then get meshes.

		static Bool Initialise(ComPtr<ID3D11Device> _pDevice,
							   ComPtr<ID3D11DeviceContext> _pDeviceContext);

		static void Shutdown(void);

    // Member Variables
	private:
		ComPtr<ID3D11Device> m_pDevice;
		ComPtr<ID3D11DeviceContext> m_pDeviceContext;

		//DX11Mesh* m_pMeshes[MAX_MESH];
};