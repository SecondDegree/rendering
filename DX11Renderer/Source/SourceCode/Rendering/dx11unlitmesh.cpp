//
//  (c) 2015 James Hannam
//
//  File Name   :   dx11unlitmesh.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Preprocessor Directives

// Library Includes

// Local Includes

// This Include
#include "dx11unlitmesh.h"

// Static Variables

// Prototypes

// Implementation
DX11UnlitMesh::DX11UnlitMesh(void)
{

}

DX11UnlitMesh::~DX11UnlitMesh(void)
{

}

const Colour&
DX11UnlitMesh::GetColour(void) const
{
	return (m_Colour);
}

void
DX11UnlitMesh::SetColour(const Colour& _Colour)
{
	m_Colour = _Colour;
}