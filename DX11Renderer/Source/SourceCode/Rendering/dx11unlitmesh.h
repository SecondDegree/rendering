//
//  (c) 2015 James Hannam
//
//  File Name   :   dx11unlitmesh.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Preprocessor Directives
#define MESH_SUBCLASS_BASIC(_Class)\
\
class _Class : public DX11UnlitMesh\
{\
	public:\
		_Class(void) {}; \
		virtual ~_Class(void) {}; \
		virtual Bool InitialiseMeshData(ComPtr<ID3D11Device> _pDevice); \
		\
	private:\
		_Class(const _Class&) = delete; \
		void operator =(const _Class&) = delete; \
};

// Library Includes
#include <SimpleMath.h>

// Local Includes
#include "dx11mesh.h"
#include "colour.h"

// Types
using DirectX::SimpleMath::Vector2;
using DirectX::SimpleMath::Vector3;

// Constants

// Prototypes

class DX11UnlitMesh abstract : public DX11Mesh
{
	// Internal Types
	protected:
		struct Vertex
		{
			Vector3 m_Position;
		};

    // Member Functions
	public:
		DX11UnlitMesh(void);
		virtual ~DX11UnlitMesh(void);

		const Colour& GetColour(void) const;

		void SetColour(const Colour& _Colour);

	protected:
		virtual Bool InitialiseMeshData(ComPtr<ID3D11Device> _pDevice) abstract;

	private:
		DX11UnlitMesh(const DX11UnlitMesh&) = delete;
		void operator =(const DX11UnlitMesh&) = delete;

	// Member Variables
	protected:
		Colour m_Colour;
};
