//
//  (c) 2014-15 James Hannam
//
//  File Name   :   colourshader.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <SimpleMath.h>
#include <sal.h>

// Local Includes
#include "types.h"
#include "dx11shader.h"

using DirectX::SimpleMath::Matrix;

// Types

// Constants

// Prototypes
struct ID3D11Buffer;

class ColourShader : public DX11Shader
{
	// Internal Types
	private:
		struct MatrixBuffer
		{
			Matrix world;
			Matrix view;
			Matrix proj;
		};

    // Member Functions
	public:
		ColourShader();
		virtual ~ColourShader();

		void Shutdown();

		Bool Apply(const Matrix& _World,
				   const Matrix& _View,
				   const Matrix& _Proj);

	private:
		ColourShader(const ColourShader&) = delete;
		ColourShader operator =(const ColourShader&) = delete;
		
		virtual Bool InitialiseShaderData(ComPtr<ID3D11Device> _pDevice,
										  HWND _In_ _pWindow);

		Bool SetShaderParameters(const Matrix& _World,
								 const Matrix& _View,
								 const Matrix& _Proj);

    // Member Variables
	private:
		ID3D11Buffer* m_pMatrixBuffer;
};