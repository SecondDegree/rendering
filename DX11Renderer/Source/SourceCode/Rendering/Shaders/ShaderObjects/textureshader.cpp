//
//  (c) 2014-15 James Hannam
//
//  File Name   :   textureshader.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <d3d11.h>
#include <cassert>

// Local Includes
#include "utility.h"

// This Include
#include "textureshader.h"

// Static Variables

// Static Function Prototypes

// Implementation
TextureShader::TextureShader()
: m_pMatrixBuffer(nullptr)
, m_pSamplerState(nullptr)
{
}

TextureShader::~TextureShader()
{
	Shutdown();
}

void
TextureShader::Shutdown()
{
	ReleaseCOMNull(m_pMatrixBuffer);
	ReleaseCOMNull(m_pSamplerState);
}

Bool
TextureShader::Apply(const Matrix& _World,
					 const Matrix& _View,
					 const Matrix& _Proj,
					 ID3D11ShaderResourceView* const _pTexture)
{
	Bool bSucceeded = false;
	Bool bMatrixDataSet = false;

	bMatrixDataSet = SetMatrixData(_World, _View, _Proj);

	if(bMatrixDataSet)
	{
		m_pDeviceContext->PSSetShaderResources(0, 1, &_pTexture);

		SetShaders();

		bSucceeded = true;
	}

	return (bSucceeded);
}

Bool
TextureShader::InitialiseShaderData(ComPtr<ID3D11Device> _pDevice,
									HWND _In_ _pWindow)
{
	assert(_pDevice != nullptr);
	assert(_pWindow != nullptr);

	Bool bSucceeded = false;
	Bool bInitialisedShadersAndLayout = false;

	bInitialisedShadersAndLayout = InitialiseShadersAndLayout(_pDevice, _pWindow);
	if(bInitialisedShadersAndLayout)
	{
		// Create the texture sampler state description;
		D3D11_SAMPLER_DESC samplerDesc;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0.0f;
		samplerDesc.BorderColor[1] = 0.0f;
		samplerDesc.BorderColor[2] = 0.0f;
		samplerDesc.BorderColor[3] = 0.0f;
		samplerDesc.MinLOD = 0.0f;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		HRESULT iResult = _pDevice->CreateSamplerState(&samplerDesc, &m_pSamplerState);

		if(SUCCEEDED(iResult))
		{
			D3D11_BUFFER_DESC matrixBufferDesc;

			// Setup the description of the dynamic matrix
				// constant buffer which is in the vertex shader.
			matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
			matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			matrixBufferDesc.MiscFlags = 0;
			matrixBufferDesc.StructureByteStride = 0;

			// Create the constant buffer pointer to
				// can access the vertex shader's constant buffer.
			iResult = _pDevice->CreateBuffer(&matrixBufferDesc, nullptr, &m_pMatrixBuffer);
			if(SUCCEEDED(iResult))
			{
				bSucceeded = true;
			}
		}
	}

	return (bSucceeded);
}

Bool
TextureShader::InitialiseShadersAndLayout(ComPtr<ID3D11Device> _pDevice,
										  HWND _In_ _pWindow)
{
	assert(_pDevice != nullptr);
	assert(_pWindow != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pWindow != nullptr)
		{
			Bool bVSCompiled = false;
			Bool bPSCompiled = false;
			Bool bVSCreated = false;
			Bool bPSCreated = false;
			Bool bLayoutCreated = false;

			ID3DBlob* pVertexShaderBuffer = nullptr;
			ID3DBlob* pPixelShaderBuffer = nullptr;

			m_ePrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

			bVSCompiled = CompileShader(SHADERTYPE_VERTEX, L"shaders//texture.vs", "VS", _pWindow, &pVertexShaderBuffer);

			bPSCompiled = CompileShader(SHADERTYPE_PIXEL, L"shaders//texture.ps", "PS", _pWindow, &pPixelShaderBuffer);

			if(bVSCompiled && bPSCompiled)
			{
				bVSCreated = InitialiseVertexShader(_pDevice, pVertexShaderBuffer);

				if(bVSCreated)
				{
					bPSCreated = InitialisePixelShader(_pDevice, pPixelShaderBuffer);

					if(bPSCreated)
					{
						// Setup the layout of data to match both what
							// is in the shader and the vertex type.
						D3D11_INPUT_ELEMENT_DESC layout[] =
						{
							{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
							{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0}
						};
		
						// Get the number of elements in the layout.
						UInt32 iNumElements = ArraySize(layout);

						bLayoutCreated = InitialiseInputLayout(_pDevice, layout, iNumElements, pVertexShaderBuffer);

						if(bLayoutCreated)
						{
							bSucceeded = true;		
						}
					}
				}
				
				ReleaseCOMNull(pVertexShaderBuffer);
				ReleaseCOMNull(pPixelShaderBuffer);
			}
		}
	}

	return (bSucceeded);
}

Bool
TextureShader::SetMatrixData(const Matrix& _World,
							 const Matrix& _View,
							 const Matrix& _Proj)
{
	assert(m_pDeviceContext != nullptr);

	Bool bSucceeded = false;

	if(m_pDeviceContext != nullptr)
	{
		if(m_pMatrixBuffer != nullptr)
		{
			HRESULT iResult = 0;
			D3D11_MAPPED_SUBRESOURCE mappedResource;

			// Transpose the matrices to prepare them for the the shader.
			// NOTE: This is a requirement of DX11.
			Matrix world = _World.Transpose();
			Matrix view = _View.Transpose();
			Matrix proj = _Proj.Transpose();

			// Lock the constant buffer so it can be written to.
			iResult = m_pDeviceContext->Map(m_pMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
			if(SUCCEEDED(iResult))
			{
				// Get a pointer to the data in the constant buffer.
				MatrixBuffer* pData = reinterpret_cast<MatrixBuffer*>(mappedResource.pData);

				// Copy the matrices into the constant buffer.
				pData->world = world;
				pData->view = view;
				pData->proj = proj;

				// Unlock the constant buffer.
				m_pDeviceContext->Unmap(m_pMatrixBuffer, 0);

				// Set the position of the constant buffer in the vertex shader.
				UInt32 iBufferNumber = 0;

				// Set the constant buffer in the vertex shader with updated values.
				m_pDeviceContext->VSSetConstantBuffers(iBufferNumber, 1, &m_pMatrixBuffer);

				bSucceeded = true;
			}
		}
	}

	return (bSucceeded);
}

void
TextureShader::SetShaders()
{
	DX11Shader::SetShaders();

	assert(m_pDeviceContext != nullptr);
	assert(m_pSamplerState != nullptr);

	if(m_pDeviceContext != nullptr)
	{
		if(m_pSamplerState != nullptr)
		{
			m_pDeviceContext->PSSetSamplers(0, 1, &m_pSamplerState);
		}
	}
}