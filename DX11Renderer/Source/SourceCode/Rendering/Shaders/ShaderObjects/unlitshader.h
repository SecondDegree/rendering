//
//  (c) 2014-15 James Hannam
//
//  File Name   :   unlitshader.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <SimpleMath.h>
#include <sal.h>

// Local Includes
#include "types.h"
#include "dx11shader.h"
#include "colour.h"

using DirectX::SimpleMath::Matrix;

// Types

// Constants

// Prototypes
struct ID3D11Buffer;

class UnlitShader : public DX11Shader
{
	// Internal Types
	private:
		struct MatrixBuffer
		{
			Matrix world;
			Matrix view;
			Matrix proj;
		};

		struct ColourBuffer
		{
			Colour colour;
		};

    // Member Functions
	public:
		UnlitShader();
		virtual ~UnlitShader();

		void Shutdown();

		Bool Apply(const Matrix& _World,
				   const Matrix& _View,
				   const Matrix& _Proj,
				   const Colour& _Colour);

	private:
		UnlitShader(const UnlitShader&) = delete;
		UnlitShader operator =(const UnlitShader&) = delete;
		
		virtual Bool InitialiseShaderData(ComPtr<ID3D11Device> _pDevice,
										  HWND _In_ _pWindow);

		Bool SetShaderParameters(const Matrix& _World,
								 const Matrix& _View,
								 const Matrix& _Proj,
								 const Colour& _Colour);

    // Member Variables
	private:
		ID3D11Buffer* m_pMatrixBuffer;
		ID3D11Buffer* m_pColourBuffer;
};