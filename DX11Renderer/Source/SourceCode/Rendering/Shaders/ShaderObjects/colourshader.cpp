//
//  (c) 2014-15 James Hannam
//
//  File Name   :   colourshader.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <D3D11.h>
#include <cassert>

// Local Includes
#include "utility.h"

// This Include
#include "colourshader.h"

// Static Variables

// Static Function Prototypes

// Implementation
ColourShader::ColourShader()
: m_pMatrixBuffer(nullptr)
{
}

ColourShader::~ColourShader()
{
	Shutdown();
}

void
ColourShader::Shutdown()
{
	ReleaseCOMNull(m_pMatrixBuffer);

	DX11Shader::Shutdown();
}

Bool
ColourShader::Apply(const Matrix& _World,
					const Matrix& _View,
					const Matrix& _Proj)
{
	Bool bSucceeded = false;

	bSucceeded = SetShaderParameters(_World, _View, _Proj);

	if(bSucceeded)
	{
		SetShaders();
	}

	return (bSucceeded);
}

// Private Functions
Bool
ColourShader::InitialiseShaderData(ComPtr<ID3D11Device> _pDevice,
								   HWND _In_ _pWindow)
{
	assert(_pDevice != nullptr);
	assert(_pWindow != nullptr);

	Bool bSucceeded = false;

	if(_pDevice != nullptr)
	{
		if(_pWindow != nullptr)
		{
			Bool bVSCompiled = false;
			Bool bPSCompiled = false;
			Bool bVSCreated = false;
			Bool bPSCreated = false;
			Bool bLayoutCreated = false;

			ID3DBlob* pVertexShaderBuffer = nullptr;
			ID3DBlob* pPixelShaderBuffer = nullptr;

			m_ePrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

			bVSCompiled = CompileShader(SHADERTYPE_VERTEX, L"shaders//colour.vs", "VS", _pWindow, &pVertexShaderBuffer);

			bPSCompiled = CompileShader(SHADERTYPE_PIXEL, L"shaders//colour.ps", "PS", _pWindow, &pPixelShaderBuffer);

			if(bVSCompiled && bPSCompiled)
			{
				bVSCreated = InitialiseVertexShader(_pDevice, pVertexShaderBuffer);

				if(bVSCreated)
				{
					bPSCreated = InitialisePixelShader(_pDevice, pPixelShaderBuffer);

					if(bPSCreated)
					{
						// Setup the layout of data to match both what
							// is in the shader and the vertex type.
						D3D11_INPUT_ELEMENT_DESC layout[] =
						{
							{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
							{"COLOUR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0}
						};
		
						// Get the number of elements in the layout.
						UInt32 iNumElements = ArraySize(layout);

						bLayoutCreated = InitialiseInputLayout(_pDevice, layout, iNumElements, pVertexShaderBuffer);

						if(bLayoutCreated)
						{
							D3D11_BUFFER_DESC matrixBufferDesc;

							// Setup the description of the dynamic matrix
								// constant buffer which is in the vertex shader.
							matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
							matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
							matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
							matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
							matrixBufferDesc.MiscFlags = 0;
							matrixBufferDesc.StructureByteStride = 0;

							// Create the constant buffer pointer so we
								// can access the vertex shader's constant buffer.
							HRESULT iResult = _pDevice->CreateBuffer(&matrixBufferDesc, nullptr, &m_pMatrixBuffer);
							if(SUCCEEDED(iResult))
							{
								bSucceeded = true;
							}
						}
					}
				}

				ReleaseCOMNull(pVertexShaderBuffer);
				ReleaseCOMNull(pPixelShaderBuffer);
			}
		}
	}

	return (bSucceeded);
}

Bool
ColourShader::SetShaderParameters(const Matrix& _World,
								  const Matrix& _View,
								  const Matrix& _Proj)
{
	assert(m_pDeviceContext != nullptr);

	Bool bSucceeded = false;

	if(m_pDeviceContext != nullptr)
	{
		if(m_pMatrixBuffer != nullptr)
		{
			HRESULT iResult = 0;
			D3D11_MAPPED_SUBRESOURCE mappedResource;

			//Matrix world;
			//Matrix view;
			//Matrix proj;

			// Transpose the matrices to prepare them for the the shader.
			// NOTE: This is a requirement of DX11.
			//D3DXMatrixTranspose(&world, &_World);
			//D3DXMatrixTranspose(&view, &_View);
			//D3DXMatrixTranspose(&proj, &_Proj);
			Matrix world = _World.Transpose();
			Matrix view = _View.Transpose();
			Matrix proj = _Proj.Transpose();

			// Lock the constant buffer so it can be written to.
			iResult = m_pDeviceContext->Map(m_pMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
			if(SUCCEEDED(iResult))
			{
				// Get a pointer to the data in the constant buffer.
				MatrixBuffer* pData = reinterpret_cast<MatrixBuffer*>(mappedResource.pData);

				// Copy the matrices into the constant buffer.
				pData->world = world;
				pData->view = view;
				pData->proj = proj;

				// Unlock the constant buffer.
				m_pDeviceContext->Unmap(m_pMatrixBuffer, 0);

				// Set the position of the constant buffer in the vertex shader.
				UInt32 iBufferNUmber = 0;

				// Set the constant buffer in the vertex shader with updated values.
				m_pDeviceContext->VSSetConstantBuffers(iBufferNUmber, 1, &m_pMatrixBuffer);

				bSucceeded = true;
			}
		}
	}

	return (bSucceeded);
}