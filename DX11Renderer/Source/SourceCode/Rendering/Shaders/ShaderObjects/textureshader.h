//
//  (c) 2014-15 James Hannam
//
//  File Name   :   textureshader.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <SimpleMath.h>
#include <sal.h>

// Local Includes
#include "types.h"
#include "dx11shader.h"

using DirectX::SimpleMath::Matrix;

// Types

// Constants

// Prototypes
struct ID3D11Buffer;
struct ID3D11SamplerState;
struct ID3D11ShaderResourceView;

class TextureShader : public DX11Shader
{
	// Internal Types
	private:
		struct MatrixBuffer
		{
			
			Matrix world;
			Matrix view;
			Matrix proj;
		};

    // Member Functions
	public:
		TextureShader();
		virtual ~TextureShader();

		void Shutdown();

		Bool Apply(const Matrix& _World,
				   const Matrix& _View,
				   const Matrix& _Proj,
				   ID3D11ShaderResourceView* const _pTexture);

	private:
		TextureShader(const TextureShader&);
		TextureShader operator =(const TextureShader&);

		virtual Bool InitialiseShaderData(ComPtr<ID3D11Device> _pDevice,
										  HWND _In_ _pWindow);

		Bool InitialiseShadersAndLayout(ComPtr<ID3D11Device> _pDevice,
										HWND _In_ _pWindow);

		Bool SetMatrixData(const Matrix& _World,
						   const Matrix& _View,
						   const Matrix& _Proj);

		virtual void SetShaders();

    // Member Variables
	private:
		ID3D11Buffer* m_pMatrixBuffer;
		ID3D11SamplerState* m_pSamplerState;
};