//
//  (c) 2014-15 James Hannam
//
//  File Name   :   dx11modeltextured.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <SimpleMath.h>

// Local Includes
#include "types.h"
#include "dx11model.h"

using DirectX::SimpleMath::Vector2;
using DirectX::SimpleMath::Vector3;

// Types

// Constants

// Prototypes
struct ID3D11ShaderResourceView;
class DX11Texture;

class DX11ModelTextured : public DX11Model
{
	// Internal Types
	private:
		struct Vertex
		{
			Vector3 m_Position;
			Vector2 m_UVCoord;
		};

    // Member Functions
	public:
		DX11ModelTextured();
		virtual ~DX11ModelTextured();

		void Shutdown();

		Bool LoadTexture(ComPtr<ID3D11Device> _pDevice,
						 const wchar_t* const _psFilename);

		// Accessors
		// Get
		ID3D11ShaderResourceView* GetTexture() const;

	protected:
		virtual Bool InitialiseModelData(ComPtr<ID3D11Device> _pDevice);

	private:
		DX11ModelTextured(const DX11ModelTextured&) = delete;
		DX11ModelTextured operator =(const DX11ModelTextured&) = delete;

    // Member Variables
	protected:
		DX11Texture* m_pTexture;
};
