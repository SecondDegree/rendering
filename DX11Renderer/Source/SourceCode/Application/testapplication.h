//
//  (c) 2015 James Hannam
//
//  File Name   :   testapplication.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <memory>

// Local Includes
#include "application.h"
#include "inputsubject.h"

// Types
#include "types.h"

// Constants

// Prototypes
class DXCamera;
class DX11ModelTextured;
class TextureShader;
class DX11MeshedAnimatedObject;
class DX11UnlitMeshObject;
class UnlitShader;

class TestApplication sealed : public Application
{
    // Member Functions
	public:
		TestApplication(void);
		virtual ~TestApplication(void);

	private:
		TestApplication(const TestApplication&) = delete;
		TestApplication& operator =(const TestApplication&) = delete;

		Bool InitialiseApplicationData(void) override;
		void Shutdown(void) override;

		void Process(Float32 _fDeltaTick) override;

		void PreDraw(void) override;
		void Draw(void) override;
		void PostDraw(void) override;

    // Member Variables
	protected:
		std::unique_ptr<DXCamera> m_pCamera;

		std::unique_ptr<DX11ModelTextured> m_pModel;
		std::unique_ptr<TextureShader> m_pTextureShader;

		std::unique_ptr<DX11UnlitMeshObject> m_pUnlitMeshObject;
		std::unique_ptr<UnlitShader> m_pUnlitShader;
};