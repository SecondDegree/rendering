//
//  (c) 2014-15 James Hannam
//
//  File Name   :   window.cpp
//  Description :   A Windows 32bit window.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Pre-Processor Directives
#define WM_WINDOW_POINTER (WM_USER + 0x0001)

// Library Includes
#include <cassert>

// Local Includes
#include "dx11renderer.h"
#include "utility.h"

// This Include
#include "window.h"

// Static Variables

// Static Function Prototypes

// Implementation
Window::Window(void)
: m_bIsFullscreen(false)
, m_WindowMode(WindowMode::Windowed)
, m_iStyle(WS_VISIBLE | WS_SYSMENU | WS_BORDER | WS_CAPTION /*| WS_MAXIMIZEBOX | WS_MINIMIZEBOX*/ /*| WS_POPUP*/)
, m_pInstance(nullptr)
, m_pWindow(nullptr)
{
}

Window::~Window(void)
{
	Shutdown();
}

Bool
Window::Initialise(HINSTANCE _pInstance,
				   const std::wstring& _sName,
				   const WindowMode _WindowMode,
				   const Size& _Size)
{
	Bool bSucceeded = false;

	if(m_pInstance == nullptr)
	{
		assert(_pInstance != nullptr);

		if(_pInstance != nullptr)
		{
			//InitialiseWindow(_pInstance, _sName);
			bSucceeded = _CreateAndRegisterWindowClass(_pInstance, _sName);

			if(bSucceeded == false) return (false);

			m_WindowedSize = _Size;
			m_ClientSize = _Size;
			SetWindowMode(_WindowMode);

			_CreateWindow(_sName);

			// Send the this pointer to the message pump so it can
			// access our message handler function.
			SendMessage(m_pWindow, WM_WINDOW_POINTER, NULL, reinterpret_cast<LPARAM>(this));

			bSucceeded = true;
		}
	}
	
	return (bSucceeded);
}

void
Window::Shutdown(void)
{
	ShutdownWindow();
}

void
Window::DisplayMessageBox(const std::wstring& _sTitle,
					      const std::wstring& _sMessage,
					      const UInt32 _iFlags)
{
	DisplayMessageBox(_sTitle, _sMessage, _iFlags, m_pWindow);;
}

void
Window::DisplayMessageBox(const wchar_t* const _psTitle,
						  const wchar_t* const _psMessage,
						  const UInt32 _iFlags)
{
	DisplayMessageBox(_psTitle, _psMessage, _iFlags, m_pWindow);
}

void
Window::DisplayMessageBox(const std::wstring& _sTitle,
					      const std::wstring& _sMessage,
					      const UInt32 _iFlags,
					      const HWND _pWindow)
{					      
	DisplayMessageBox(_sTitle.c_str(), _sMessage.c_str(), _iFlags, _pWindow);
}

void
Window::DisplayMessageBox(const wchar_t* const _psTitle,
						  const wchar_t* const _psMessage,
						  const UInt32 _iFlags,
						  const HWND _pWindow)
{
	MessageBoxW(_pWindow, _psMessage, _psTitle, _iFlags);
}

LRESULT CALLBACK
Window::MessageHandler(HWND _pWindow, UINT _iMessage, WPARAM _wparam, LPARAM _lparam)
{
	LRESULT iResult = 0;

	switch(_iMessage)
	{
		case WM_MOUSELEAVE: // Fall through.
		case WM_KILLFOCUS: // Fall through.
		case WM_SYSKEYDOWN: // Fall through.
		case WM_SYSKEYUP:
		{
			m_InputSubject.ResetKeys();
			break;
		}
		case WM_KEYDOWN:
		{
			m_InputSubject.FireKeyboardPress(LOWORD(_wparam), KEYSTATE_DOWN);
			break;
		}
		case WM_KEYUP:
		{
			m_InputSubject.FireKeyboardPress(LOWORD(_wparam), KEYSTATE_UP);
			break;
		}
		case WM_LBUTTONDOWN:
		{
			m_InputSubject.FireMousePress(KEY_MOUSELEFT, KEYSTATE_DOWN);
			break;
		}
		case WM_LBUTTONUP:
		{
			m_InputSubject.FireMousePress(KEY_MOUSELEFT, KEYSTATE_UP);
			break;
		}
		case WM_MBUTTONDOWN:
		{
			m_InputSubject.FireMousePress(KEY_MOUSEMIDDLE, KEYSTATE_DOWN);
			break;
		}
		case WM_MBUTTONUP:
		{
			m_InputSubject.FireMousePress(KEY_MOUSEMIDDLE, KEYSTATE_UP);
			break;
		}
		case WM_RBUTTONDOWN:
		{
			m_InputSubject.FireMousePress(KEY_MOUSERIGHT, KEYSTATE_DOWN);
			break;
		}
		case WM_RBUTTONUP:
		{
			m_InputSubject.FireMousePress(KEY_MOUSERIGHT, KEYSTATE_UP);
			break;
		}
		case WM_MOUSEMOVE:
		{
			m_InputSubject.FireMouseMove(GET_X_LPARAM(_lparam), GET_Y_LPARAM(_lparam));
			break;
		}
		default:
		{
			iResult = DefWindowProc(_pWindow, _iMessage, _wparam, _lparam);
			break;
		}
	}

	return (iResult);
}

// Accessors
// Get
Int32
Window::GetDisplayWidth(void)
{
	return (GetSystemMetrics(SM_CXSCREEN));
}

Int32
Window::GetDisplayHeight(void)
{
	return (GetSystemMetrics(SM_CYSCREEN));
}

HWND
Window::GetHandle(void) const
{
	return (m_pWindow);
}

Int32
Window::GetClientWidth(void) const
{
	return (m_iClientWidth);
}

Int32
Window::GetClientHeight(void) const
{
	return (m_iClientHeight);
}

Float32
Window::GetClientWidthF(void) const
{
	return (static_cast<Float32>(GetClientWidth()));
}

Float32
Window::GetClientHeightF(void) const
{
	return (static_cast<Float32>(GetClientHeight()));
}

const Size&
Window::GetClientSize(void) const
{
	return (m_ClientSize);
}

const Size&
Window::GetWindowSize(void) const
{
	return (m_WindowedSize);
}

Bool
Window::GetIsFullscreen(void) const
{
	return (m_WindowMode == WindowMode::Fullscreen);
}

WindowMode
Window::GetWindowMode(void) const
{
	return (m_WindowMode);
}

// Set
void
Window::SetWindowMode(WindowMode _WindowMode)
{
	switch(_WindowMode)
	{
		case(WindowMode::Windowed):
		{
			_SetWindowed();
			break;
		}
		case(WindowMode::Fullscreen) :
		{
			_SetFullscreen();
			break;
		}
		case(WindowMode::Borderless) :
		{
			_SetBorderless();
			break;
		}
		default:
		{
			assert(false); // Error - invalid window mode, somehow.
		}
	}
}

void
Window::SetClientSize(Int32 _iWidth, Int32 _iHeight)
{
	if(m_iClientWidth != _iWidth && m_iClientHeight != _iHeight)
	{
		m_iClientWidth = _iWidth;
		m_iClientHeight = _iHeight;

		_AdjustWindowRect();

		// TODO: Fire on window size changed - or something.
	}
}

void
Window::SetClientSize(const Size& _Size)
{
	SetClientSize(_Size.width, _Size.height);
}

Bool
Window::_CreateAndRegisterWindowClass(HINSTANCE _pInstance, const std::wstring& _sName)
{
	if(m_sWindowClassName.empty())
	{
		WNDCLASSEXW windowClass;

		m_pInstance = _pInstance;
		m_sWindowClassName = _sName;

		// Setup the windows class with default settings.
		windowClass.cbSize = sizeof(windowClass);
		windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		windowClass.lpfnWndProc = WndProc;
		windowClass.cbClsExtra = NULL; // Can be used for custom data.
		windowClass.cbWndExtra = NULL;
		windowClass.hInstance = m_pInstance;
		windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		windowClass.hIconSm = windowClass.hIcon;
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		windowClass.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
		windowClass.lpszMenuName = NULL;
		windowClass.lpszClassName = m_sWindowClassName.c_str();

		// Register the window class.
		RegisterClassEx(&windowClass);

		return (true);
	}

	return (false);
}

void
Window::_CreateWindow(const std::wstring& _sWindowName)
{
	_AdjustWindowRect();

	Int32 iPosX = (GetDisplayWidth() - m_iWindowWidth) / 2;
	Int32 iPosY = (GetDisplayHeight() - m_iWindowHeight) / 2;

	// Create the window with the screen
	// settings and get the handle to it.
	m_pWindow = CreateWindowEx(WS_EX_APPWINDOW, // Window style
							   m_sWindowClassName.c_str(), // Class name
							   _sWindowName.c_str(), // Window name
							   // TODO: Add WS_THICKFRAME for resizing the window.
							   //	Currently works but isn't pretty as the screen goes black while resizing - should just stop rendering and not clear the frame.
							   WS_VISIBLE | WS_SYSMENU | WS_BORDER | WS_CAPTION /*| WS_MAXIMIZEBOX | WS_MINIMIZEBOX*/ /*| WS_POPUP*/, // Style values
							   iPosX, // X position - top left
							   iPosY, // Y position - top left
							   m_iWindowWidth,
							   m_iWindowHeight,
							   NULL,
							   NULL,
							   m_pInstance,
							   NULL);

	// Bring the the window up on the screen and set it as main focus.
	//ShowWindow(m_pWindow, SW_SHOW);
	//SetForegroundWindow(m_pWindow);
	//SetFocus(m_pWindow);
}

//void
//Window::InitialiseWindow(HINSTANCE _pInstance, LPCWSTR _psName)
//{
	//WNDCLASSEXW windowClass;
	//Int32 iPosX = 0;
	//Int32 iPosY = 0;
	//
	//m_pInstance = _pInstance;
	//m_psName = _psName;

	//// Setup the windows class with default settings.
	//windowClass.cbSize = sizeof(windowClass);
	//windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	//windowClass.lpfnWndProc = WndProc;
	//windowClass.cbClsExtra = NULL; // Can be used for custom data.
	//windowClass.cbWndExtra = NULL;
	//windowClass.hInstance = m_pInstance;
	//windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	//windowClass.hIconSm = windowClass.hIcon;
	//windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	//windowClass.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	//windowClass.lpszMenuName = NULL;
	//windowClass.lpszClassName = m_psName;

	//// Register the window class.
	//RegisterClassEx(&windowClass);

	//// Setup the screen settings depending on whether
	//// it is running in full screen or windowed mode.
	//if(m_bIsFullscreen)
	//{
	//	DEVMODE screenSettings;
	//	m_iWidth = GetDisplayWidth();
	//	m_iHeight = GetDisplayHeight();

	//	SecureZeroMemory(&screenSettings, sizeof(screenSettings));

	//	screenSettings.dmSize = sizeof(screenSettings);
	//	screenSettings.dmPelsWidth = static_cast<DWORD>(m_iWidth);
	//	screenSettings.dmPelsHeight = static_cast<DWORD>(m_iHeight);
	//	screenSettings.dmBitsPerPel = 32; // 32 bit.
	//	screenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	//	// Change the display settings to full screen.
	//	ChangeDisplaySettings(&screenSettings, 0);
	//}
	//else
	//{
	//	// TODO: Take width and height as parameters.
	//	m_iWidth = 800;
	//	m_iHeight = 600;

	//	// Position the window at the centre of the screen.
	//	iPosX = (m_iDisplayWidth - m_iWidth) / 2;
	//	iPosY = (m_iDisplayHeight - m_iHeight) / 2;
	//}

	//// Create the window with the screen
	//// settings and get the handle to it.
	//m_pWindow = CreateWindowEx(WS_EX_APPWINDOW, // Window style
	//						   m_psName, // Class name
	//						   m_psName, // Window name
	//						   WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP, // Style values
	//						   iPosX, // X position - top left
	//						   iPosY, // Y position - top left
	//						   m_iWidth,
	//						   m_iHeight,
	//						   NULL,
	//						   NULL,
	//						   m_pInstance,
	//						   NULL);

	//// Bring the the window up on the screen and set it as main focus.
	//ShowWindow(m_pWindow, SW_SHOW);
	//SetForegroundWindow(m_pWindow);
	//SetFocus(m_pWindow);

	//// Hide the mouse cursor.
	////ShowCursor(FALSE);
	//
	//// Send the this pointer to the message pump so it can
	//	// access to our message handler function.
	//SendMessage(m_pWindow, WM_WINDOW_POINTER, NULL, reinterpret_cast<LPARAM>(this));
//}

void
Window::ShutdownWindow(void)
{
	if(m_pWindow != nullptr)
	{
		// Show the mouse cursor.
		ShowCursor(TRUE);

		// Fix the display settings if leaving full screen mode.
		if(m_WindowMode == WindowMode::Fullscreen)
		{
			ChangeDisplaySettings(NULL, 0);
		}

		// Remove the window.
		DestroyWindow(m_pWindow);
		m_pWindow = nullptr;

		// Remove the application instance.
		UnregisterClass(m_sWindowClassName.c_str(), m_pInstance);
		m_pInstance = nullptr;
		m_sWindowClassName.clear();
	}
}

void
Window::_SetFullscreen(void)
{
	DEVMODE screenSettings;
	SetClientSize(GetDisplayWidth(), GetDisplayHeight());

	SecureZeroMemory(&screenSettings, sizeof(screenSettings));

	screenSettings.dmSize = sizeof(screenSettings);
	screenSettings.dmPelsWidth = static_cast<DWORD>(m_iClientWidth);
	screenSettings.dmPelsHeight = static_cast<DWORD>(m_iClientHeight);
	screenSettings.dmBitsPerPel = 32; // 32 bit.
	screenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	// Change the display settings to full screen.
	ChangeDisplaySettings(&screenSettings, 0);

	m_WindowMode = WindowMode::Fullscreen;
}

void
Window::_SetWindowed(void)
{
	SetClientSize(m_WindowedSize);

	if(m_WindowMode == WindowMode::Fullscreen)
	{
		ChangeDisplaySettings(NULL, 0);

		// Position the window at the centre of the screen.
		Int32 iPosX = (GetDisplayWidth() - m_iWindowWidth) / 2;
		Int32 iPosY = (GetDisplayHeight() - m_iWindowHeight) / 2;
	}

	m_WindowMode = WindowMode::Windowed;
}

void
Window::_SetBorderless(void)
{
	SetClientSize(m_WindowedSize);

	if(m_WindowMode == WindowMode::Fullscreen)
	{
		ChangeDisplaySettings(NULL, 0);

		// Position the window at the centre of the screen.
		Int32 iPosX = (GetDisplayWidth() - m_iWindowWidth) / 2;
		Int32 iPosY = (GetDisplayHeight() - m_iWindowHeight) / 2;
	}

	m_WindowMode = WindowMode::Borderless;
}

void
Window::_AdjustWindowRect(void)
{
	RECT windowRect = {0, 0, m_iClientWidth, m_iClientHeight};
	AdjustWindowRect(&windowRect, m_iStyle, FALSE);

	m_WindowSize = {windowRect.right - windowRect.left, windowRect.bottom - windowRect.top};
}

LRESULT CALLBACK WndProc(HWND _pWindow, UINT _iMessage, WPARAM _wparam, LPARAM _lparam)
{
	LRESULT iResult = 0;
	static Window* s_pWindow = nullptr;

	switch(_iMessage)
	{
		case WM_WINDOW_POINTER:
		{
			s_pWindow = reinterpret_cast<Window*>(_lparam);
			break;
		}
		case WM_CLOSE: // Fall through.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			break;
		}
		// Messages we don't handle.
		default:
		{
			if(s_pWindow != nullptr)
			{
				iResult = s_pWindow->MessageHandler(_pWindow, _iMessage, _wparam, _lparam);
			}
			else
			{
				iResult = DefWindowProc(_pWindow, _iMessage, _wparam, _lparam);
			}
			break;
		}
	}

	return (iResult);
}