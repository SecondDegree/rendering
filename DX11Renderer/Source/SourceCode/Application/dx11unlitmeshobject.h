//
//  (c) 2015 James Hannam
//
//  File Name   :   dx11unlitmeshobject.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Preprocessor Directives

// Library Includes
#include <memory>

// Local Includes
#include "dxobject.h"
#include "colour.h"

// Types

// Constants

// Prototypes
class DX11UnlitMesh;

class DX11UnlitMeshObject : public DXObject
{
    // Member Functions
	public:
		DX11UnlitMeshObject(void);
		virtual ~DX11UnlitMeshObject(void);

		virtual void Process(Float32 _fDeltaTick);
		void Draw(void);

		Colour GetColour(void) const;

		void SetMesh(std::shared_ptr<DX11UnlitMesh> _pMesh);
		void SetColour(const Colour& _Colour);

	private:
		DX11UnlitMeshObject(const DX11UnlitMeshObject&) = delete;
		void operator =(const DX11UnlitMeshObject&) = delete;

    // Member Variables
	private:
		std::shared_ptr<DX11UnlitMesh> m_pMesh;
};
