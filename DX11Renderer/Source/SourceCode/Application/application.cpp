//
//  (c) 2014-15 James Hannam
//
//  File Name   :   application.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <Windows.h>
#include <SimpleMath.h>
#include <math.h>

// Local Includes
#include "utility.h"
#include "window.h"
#include "dx11renderer.h"
#include "dxcamera.h"
#include "dx11modeltextured.h"
#include "textureshader.h"
#include "mathutility.h"
#include "clock.h"

#include "squaremesh.h"
#include "unlitshader.h"

// This Include
#include "application.h"

using DirectX::SimpleMath::Matrix;

// Static Variables

// Static Function Prototypes

// Implementation
Application::Application()
: m_pWindow(nullptr)
, m_pRenderer(nullptr)
{
}

Application::~Application()
{
	Shutdown();
}

Bool
Application::Initialise(HINSTANCE _pInstance,
						LPCWSTR _psName)
{
	Bool bSucceeded = false;
	Bool bWindowInitialised = false;

	m_pClock = std::make_unique<Clock>();

	if(m_pClock != nullptr) m_pClock->Process();

	m_pWindow = std::make_unique<Window>();
	bWindowInitialised = m_pWindow->Initialise(_pInstance, _psName);

	if(bWindowInitialised)
	{
		Bool bRendererInitialised = false;

		m_pRenderer = std::make_unique<DX11Renderer>();
		bRendererInitialised = m_pRenderer->Initialise(*m_pWindow, Colour::Black());

		if(bRendererInitialised)
		{
			bSucceeded = true;

			InitialiseApplicationData();
		}
	}

	return (bSucceeded);
}

void
Application::Shutdown()
{
	
}

void
Application::Run()
{
	MSG message;
	Bool bShouldExit = false;
	
	SecureZeroMemory(&message, sizeof(message));

	// Loop until we receive the quit message.
	while(bShouldExit == false)
	{
		// Handle the windows message pump.
		if(PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}

		// If we've received the windows quit message.
		if(message.message == WM_QUIT)
		{
			bShouldExit = true;
		}
		else
		{
			_ExecuteOneFrame();
		}
	}
}

// Private Functions.
void
Application::_ExecuteOneFrame()
{
	Float32 fDeltaTick = (m_pClock != nullptr) ? m_pClock->GetDeltaTick() : 0.0f;

	Process(fDeltaTick);

	if(m_pClock != nullptr) m_pClock->Process();
	
	if(m_pRenderer != nullptr)
	{
		_PreDraw();
		Draw();
		_PostDraw();
	}
}

void
Application::_PreDraw(void)
{
	m_pRenderer->BeginRender();

	PreDraw();
}

void
Application::_PostDraw(void)
{
	PostDraw();

	m_pRenderer->EndRender();
}