//
//  (c) 2014-15 James Hannam
//
//  File Name   :   application.h
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <memory>

// Local Includes
#include "inputsubject.h"

// Types
#include "types.h"

// Constants

// Prototypes
class Window;
class DX11Renderer;
class Clock;

// Really a DX11Application
class Application abstract // NOTE: This is basically the Game class.
{
    // Member Functions
	protected:
		Application(void);

	public:
		virtual ~Application(void);

		Bool Initialise(HINSTANCE _pInstance,
						LPCWSTR _psName);
		virtual void Shutdown(void);
		
		void Run(void);

	protected:
		virtual Bool InitialiseApplicationData(void) abstract;

		virtual void Process(Float32 _fDeltaTick) abstract;

		// These draw functions will not be called if there is no renderer.
		// Called after BeginRender
		virtual void PreDraw(void) abstract;
		virtual void Draw(void) abstract;
		// Called before EndRender
		virtual void PostDraw(void) abstract;

	private:
		Application(const Application&) = delete;
		Application& operator =(const Application&) = delete;
		
		void _ExecuteOneFrame(void);

		void _PreDraw(void);
		void _PostDraw(void);

    // Member Variables
	protected:
		InputSubject m_InputSubject;

		std::unique_ptr<Window> m_pWindow; // Would be nice to support multiple windows.

		std::unique_ptr<Clock> m_pClock;

		std::unique_ptr<DX11Renderer> m_pRenderer;
};