//
//  Bachelor of Software Engineering
//  Media Design School
//  Auckland
//  New Zealand
//
//  (c) 2013 Media Design School
//
//  File Name   :   inputsubject.h
//  Description :   Interprets input from the windows message pump and sends it to subscribers.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

#ifndef __INPUTSUBJECT_H__
#define __INPUTSUBJECT_H__

// Library Includes
#include <Windows.h>
#include <vector>
#include <map>

// Local Includes
#include "types.h"

// Types

// Constants
enum EKeyState
{
	INVALID_KEYSTATE = -1,
	KEYSTATE_DOWN,
	KEYSTATE_UP,
	MAX_KEYSTATE
};

enum EKey
{
	INVALID_KEY = -1,
	// Function Keys
	KEY_F1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	//KEY_F10, UNSUPPORTED
	KEY_F11,
	KEY_F12,
	// Arrow Keys
	KEY_ARROWLEFT,
	KEY_ARROWUP,
	KEY_ARROWRIGHT,
	KEY_ARROWDOWN,
	// Number Pad Keys
	KEY_NUM0,
	KEY_NUM1,
	KEY_NUM2,
	KEY_NUM3,
	KEY_NUM4,
	KEY_NUM5,
	KEY_NUM6,
	KEY_NUM7,
	KEY_NUM8,
	KEY_NUM9,
	KEY_MULTIPLY,
	KEY_ADD,
	KEY_SEPARATOR,
	KEY_SUBTRACT,
	KEY_DECIMAL,
	KEY_DIVIDE,
	// Mouse Keys
	KEY_MOUSELEFT,
	KEY_MOUSEMIDDLE,
	KEY_MOUSERIGHT,
	// Numerical Keys
	KEY_0 = '0',
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	// Alphabetical Keys
	KEY_A = 'A',
	KEY_B,
	KEY_C,
	KEY_D,
	KEY_E,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_I,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_M,
	KEY_N,
	KEY_O,
	KEY_P,
	KEY_Q,
	KEY_R,
	KEY_S,
	KEY_T,
	KEY_U,
	KEY_V,
	KEY_W,
	KEY_X,
	KEY_Y,
	KEY_Z,
	// Other Keys
	KEY_ESCAPE,
	KEY_SPACE,
	KEY_ENTER,
	KEY_BACKSPACE,
	KEY_SHIFT,
	KEY_CONTROL,
	//KEY_ALT, UNSUPPORTED
	KEY_TAB,
	KEY_HOME,
	KEY_END,
	KEY_INSERT,
	KEY_DELETE,
	KEY_PAGEUP,
	KEY_PAGEDOWN,
	KEY_PLUS,
	KEY_MINUS,
	KEY_COMMA,
	KEY_PERIOD,
	KEY_COLON,
	KEY_QUOTE,
	KEY_BRACKETOPEN,
	KEY_BRACKETCLOSE,
	KEY_TILD,
	KEY_FORWARDSLASH,
	KEY_BACKSLASH,
	//KEY_CAPSLOCK, UNSUPPORTED
	//KEY_NUMLOCK, UNSUPPORTED
	//KEY_SCROLLLOCK, UNSUPPORTED
	//KEY_PRINTSCREEN, UNSUPPORTED
	//KEY_BREAK, UNSUPPORTED
	MAX_KEY
};

// TODO: Add support for ALT and F10 keys (syskey).
// TODO: Add support for all remaining keys
	// (caps lock, num lock, print screen, scroll lock, break).
// TODO: Add mousewheel scroll support.
// TODO: Add support for differentiation between left and right shift/ctrl/alt keys.

// Prototypes
class InputObserver;

class InputSubject
{
	// TODO: Make this a singleton.

    // Member Functions
	public:
		InputSubject();
		~InputSubject();

		static void Initialise();
		void ResetKeys();

		void FireKeyboardPress(const WORD _iKey, const EKeyState _eKeyState);
		void FireMousePress(const EKey _eMouseKey, const EKeyState _eKeyState);
		void FireMouseMove(const Int32 _iXPosition, const Int32 _iYPosition);
		void FireFileDrop(const Int8* const _piFilepath,
						  const Int8* const _piFileExtension);

		// TODO: Consider using reference counting
		// for subscribers to avoid access violations.
		static Bool Subscribe(InputObserver* _In_ _pObserver);
		static Bool Unsubscribe(InputObserver* _In_ _pObserver);

		// Accessors
		// Get
		static EKeyState GetKeyState(EKey _eKey);
		static Int32 GetCursorXPosition();
		static Int32 GetCursorYPosition();

	private:
		InputSubject(const InputSubject&);
		InputSubject operator =(const InputSubject&);

		// Key Conversion Functions
		EKey ConvertToKey(const WORD _iKey);
		EKey ConvertNumberToKey(const WORD _iNumber);
		EKey ConvertFunctionToKey(const WORD _iFunction);
		EKey ConvertCharacterToKey(const WORD _iCharacter);
		EKey ConvertArrowToKey(const WORD _iArrow);
		EKey ConvertNumberpadToKey(const WORD _iNumberpad);
		EKey ConvertOtherToKey(const WORD _iOther);

    // Member Variables
	private:
	// TODO: Rework this to be a singleton rather than entirely static - saves stack memory.
		// Variables
		static Bool sm_bIsInitialised;
		static Int32 sm_iCursorXPosition;
		static Int32 sm_iCursorYPosition;

		// Containers
		static std::vector<InputObserver*> sm_Observers;
		static std::map<EKey, EKeyState> sm_KeyStates;
};

#endif // __INPUTSUBJECT_H__
