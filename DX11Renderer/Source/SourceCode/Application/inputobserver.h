//
//  Bachelor of Software Engineering
//  Media Design School
//  Auckland
//  New Zealand
//
//  (c) 2013 Media Design School
//
//  File Name   :   inputobserver.h
//  Description :   Provides an abstraction of an object which can recieve input messages.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

#ifndef __INPUTOBSERVER_H__
#define __INPUTOBSERVER_H__

// Library Includes
#include <Windows.h>
#include <map>

// Local Includes
#include "types.h"

// Types

// Constants

// Prototypes
enum EKey;
enum EKeyState;

class InputObserver
{
    // Member Functions
	public:
		~InputObserver();

		// TODO: Consider making these protected and friending the input subject...
		virtual void OnKeyboardPress(const EKey _eKey,
									 const EKeyState _eState);
		virtual void OnMousePress(const EKey _eMouseKey,
								  const EKeyState _eState);
		virtual void OnMouseMove(const Int32 _iXPosition,
								 const Int32 _iYPosition);
		virtual void OnFileDrop(const Int8* const _piFilepath,
								const Int8* const _piFileExtension);

		// TODO: Consider using bit flags to allow observers to
		// subscribe to specific events instead of only all events.

	protected:
		InputObserver();

	private:
		InputObserver(const InputObserver& _krRHS);
		InputObserver operator =(const InputObserver& _krRHS);

    // Member Variables
	private:
};

#endif // __INPUTOBSERVER_H__
