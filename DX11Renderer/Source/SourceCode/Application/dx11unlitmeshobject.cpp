//
//  (c) 2015 James Hannam
//
//  File Name   :   dx11unlitmeshobject.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Preprocessor Directives

// Library Includes

// Local Includes
#include "dx11unlitmesh.h"

// This Include
#include "dx11unlitmeshobject.h"

// Static Variables

// Prototypes

// Implementation
DX11UnlitMeshObject::DX11UnlitMeshObject(void)
{

}

DX11UnlitMeshObject::~DX11UnlitMeshObject(void)
{

}

void
DX11UnlitMeshObject::Process(Float32 _fDeltaTick)
{
	DXObject::Process(_fDeltaTick);
}

void
DX11UnlitMeshObject::Draw(void)
{
	if(m_pMesh != nullptr)
	{
		m_pMesh->SetBuffers();

		m_pMesh->Draw();
	}
}

Colour
DX11UnlitMeshObject::GetColour(void) const
{
	if(m_pMesh != nullptr) return (m_pMesh->GetColour());
	return (Colour::Black());
}

void
DX11UnlitMeshObject::SetMesh(std::shared_ptr<DX11UnlitMesh> _pMesh)
{
	m_pMesh = _pMesh;
}

void
DX11UnlitMeshObject::SetColour(const Colour& _Colour)
{
	if(m_pMesh != nullptr) m_pMesh->SetColour(_Colour);
}