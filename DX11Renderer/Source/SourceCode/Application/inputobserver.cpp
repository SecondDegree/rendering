//
//  Bachelor of Software Engineering
//  Media Design School
//  Auckland
//  New Zealand
//
//  (c) 2013 Media Design School
//
//  File Name   :   inputobserver.cpp
//  Description :   Provides an abstraction of an object which can recieve input messages.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes

// Local Includes
//#include "inputsubject.h"

// This Include
#include "inputobserver.h"

// Static Variables

// Static Function Prototypes

// Implementation

/**
*
* This function builds the object
*
* @author James Hannam
*
*/
InputObserver::InputObserver()
{
}

/**
*
* This function destroys the object
*
* @author James Hannam
*
*/
InputObserver::~InputObserver()
{
}

/**
*
* This function does nothing (to be overridden).
*
* @author James Hannam
*
*/
void
InputObserver::OnKeyboardPress(const EKey _eKey,
								const EKeyState _eState)
{
}

/**
*
* This function does nothing (to be overridden).
*
* @author James Hannam
*
*/
void
InputObserver::OnMousePress(const EKey _eMouseKey, const EKeyState _eState)
{
}

/**
*
* This function does nothing (to be overridden).
*
* @author James Hannam
*
*/
void
InputObserver::OnMouseMove(const Int32 _iXPosition, const Int32 _iYPosition)
{
}

/**
*
* This function does nothing (to be overridden).
*
* @author James Hannam
*
*/
void
InputObserver::OnFileDrop(const Int8* const _piFilepath,
							const Int8* const _piFileExtension)
{
}