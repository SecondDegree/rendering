//
//  (c) 2014-15 James Hannam
//
//  File Name   :   window.h
//  Description :   A Windows 32bit window.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <WindowsX.h>
#include <memory>
#include <string>

// Local Includes
#include "types.h"
#include "inputsubject.h"

// Types
enum class WindowMode : Int8 // TODO: Finish implementing window mode functionality.
{
	Windowed = 0,
	Fullscreen,
	Borderless // Borderless window (full screen size).
};

struct Position
{
	Int32 x = 0;
	Int32 y = 0;

	Position() = default;
	Position(const Int32 _x, const Int32 _y) : x(_x), y(_y) {}

	Bool IsZero(void) const
	{
		return (x == y && x == 0);
	}
};

struct Size
{
	Int32 width = 0;
	Int32 height = 0;

	Size() = default;
	Size(const Int32 _width, const Int32 _height) : width(_width), height(_height) {}
};

// Constants

// Prototypes
static LRESULT CALLBACK WndProc(HWND _pWindow,
								UINT _iMessage,
								WPARAM _wparam,
								LPARAM _lparam);

class Window
{
	// TODO: Design a way to handle message boxes as a part of this.

	// TODO: Make this class abstract and inherit a DX11Window from it

    // Member Functions
	public:
		Window(void);
		~Window(void);

		Window(const Window&) = delete;
		Window operator =(const Window&) = delete;

		/*
		* Size - size of the client area (inside the window)
		*/
		Bool Initialise(HINSTANCE _pInstance,
						const std::wstring& _sName,
						const WindowMode _WindowMode = WindowMode::Windowed,
						const Size& _Size = {1280, 720});
		void Shutdown(void);

		void DisplayMessageBox(const std::wstring& _sTitle,
							   const std::wstring& _sMessage,
							   const UInt32 _iFlags);

		void DisplayMessageBox(const wchar_t* const _psTitle,
							   const wchar_t* const _psMessage,
							   const UInt32 _iFlags);

		static inline void DisplayMessageBox(const std::wstring& _sTitle,
											 const std::wstring& _sMessage,
											 const UInt32 _iFlags,
											 const HWND _pWindow = nullptr);

		static inline void DisplayMessageBox(const wchar_t* const _psTitle,
											 const wchar_t* const _psMessage,
											 const UInt32 _iFlags,
											 const HWND _pWindow = nullptr);


		LRESULT CALLBACK MessageHandler(HWND _pWindow,
										UINT _iMessage,
										WPARAM _wparam,
										LPARAM _lparam);

		// Accessors
		// Get
		static inline Int32 GetDisplayWidth(void);
		static inline Int32 GetDisplayHeight(void);

		HWND GetHandle(void) const;

		Int32 GetClientWidth(void) const;
		Int32 GetClientHeight(void) const;

		Float32 GetClientWidthF(void) const;
		Float32 GetClientHeightF(void) const;

		const Size& GetClientSize(void) const;

		const Size& GetWindowSize(void) const;

		Bool GetIsFullscreen(void) const;

		WindowMode GetWindowMode(void) const;

		// Set
		void SetWindowMode(WindowMode _WindowMode);

		void SetClientSize(Int32 _iWidth, Int32 _iHeight);
		void SetClientSize(const Size& _Size);

		// TODO: Create a hook back to the application to send messages
		//	when the window size and such things change.
		//	This will be necessary to resize the renderer.

	private:
		Bool _CreateAndRegisterWindowClass(HINSTANCE _pInstance, const std::wstring& _sName);
		void _CreateWindow(const std::wstring& _sWindowName);
		void ShutdownWindow(void);

		void _SetFullscreen(void);
		void _SetWindowed(void);
		void _SetBorderless(void);

		void _AdjustWindowRect(void);

    // Member Variables
	private:
		Bool m_bIsFullscreen;
		WindowMode m_WindowMode;

		union
		{
			struct
			{
				Int32 m_iClientWidth;
				Int32 m_iClientHeight;
			};
			struct 
			{
				Size m_ClientSize;
			};
		};

		union
		{
			struct
			{
				Int32 m_iWindowWidth;
				Int32 m_iWindowHeight;
			};
			struct
			{
				Size m_WindowSize;
			};
		};

		// WindowSize
		// ClientArea

		DWORD m_iStyle;

		Size m_WindowedSize;

		std::wstring m_sWindowClassName;
		HINSTANCE m_pInstance;
		HWND m_pWindow;
		MSG m_Message;

		InputSubject m_InputSubject;
};
