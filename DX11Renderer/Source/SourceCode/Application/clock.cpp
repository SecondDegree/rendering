//
//  Diploma of Interactive Gaming
//  Media Design School
//  Auckland
//  New Zealand
//
//  (c) 2012 Media Design School
//
//  File Name   :   clock.cpp
//  Description :   The game's clock object. Provides time keeping functionality.
//  Author      :   Team Green
//  Mail        :   tacticscubed@gmail.com
//

// Pre-Processor Directives

// Library Includes

// Local Includes

// This Include
#include "clock.h"

// Static Variables

// Static Function Prototypes

// Implementation
/**
*
* This function constructs the object.
* (Task ID: N/A)
*
* @author James Hannam.
*
*/
Clock::Clock()
: m_rMaxDeltaTick(1.0f/10.0f)
, m_rTimeElapsed(0.0)
, m_rDeltaTime(0.0)
, m_rTimerStartTime(0.0)
, m_rTimerEndTime(0.0)
, m_rLastTime(0.0f)
, m_rCurrentTime(0.0f)
, m_bIsPerformanceTimerActive(false)
{
	SecureZeroMemory(&m_CountsPerSecond, sizeof(m_CountsPerSecond));
	SecureZeroMemory(&m_LastTime, sizeof(m_LastTime));
	SecureZeroMemory(&m_CurrentTime, sizeof(m_CurrentTime));
	SecureZeroMemory(&m_PerformanceTimer, sizeof(m_PerformanceTimer));

	QueryPerformanceFrequency(&m_CountsPerSecond);
}

/**
*
* This function destroys the object.
* (Task ID: N/A)
*
* @author James Hannam.
*
*/
Clock::~Clock()
{
}

/**
*
* This function calculates the current delta time.
* (Task ID: N/A)
*
* @author James Hannam.
*
*/
void
Clock::Process()
{
	QueryPerformanceFrequency(&m_CountsPerSecond);

	m_LastTime = m_CurrentTime;

	QueryPerformanceCounter(&m_CurrentTime);

	if(m_LastTime.QuadPart == 0)
	{
		m_LastTime = m_CurrentTime;
	}
	else
	{
		// Do nothing.
	}

	m_rDeltaTime = (m_CurrentTime.QuadPart - m_LastTime.QuadPart) *
					(1000.0 / m_CountsPerSecond.QuadPart);

	m_rTimeElapsed += m_rDeltaTime;
}

/**
*
* This function starts timing.
* (Task ID: N/A)
*
* @author James Hannam.
* @return Returns the success of the timer starting.
*
*/
Bool
Clock::StartPerformanceTimer()
{
	bool bResult = false;

	if(!m_bIsPerformanceTimerActive)
	{
		m_bIsPerformanceTimerActive = true;

		QueryPerformanceCounter(&m_PerformanceTimer);

		m_rTimerStartTime = static_cast<Float64>(m_PerformanceTimer.QuadPart) /
							static_cast<Float64>(m_CountsPerSecond.QuadPart);
		m_rTimerEndTime = 0;

		bResult = true;
	}
	else
	{
		// Do nothing.
	}

	return (bResult);
}

/**
*
* This function stops timing and returns
the time the timer took to end.
* (Task ID: N/A)
*
* @author James Hannam.
* @return Returns the time, in seconds, that it took to stop timing.
*
*/
Float64
Clock::StopPerformanceTimer()
{
	m_bIsPerformanceTimerActive = false;

	QueryPerformanceCounter(&m_PerformanceTimer);

	m_rTimerEndTime = static_cast<Float64>(m_PerformanceTimer.QuadPart) /
						static_cast<Float64>(m_CountsPerSecond.QuadPart);

	Float64 dTimerDelta = m_rTimerEndTime - m_rTimerStartTime;

	return (dTimerDelta);
}

// Accessors
// Get
/**
*
* This function calculates the current delta time.
* (Task ID: N/A)
*
* @author James Hannam.
* @return Returns the current delta tick in seconds.
*
*/
Float32
Clock::GetDeltaTick() const
{
	Float32 fCurrentDeltaTick = static_cast<Float32>(m_rDeltaTime * 0.001);

	// Cap the delta tick.
	if(fCurrentDeltaTick > m_rMaxDeltaTick) fCurrentDeltaTick = m_rMaxDeltaTick;

	return (fCurrentDeltaTick);
}