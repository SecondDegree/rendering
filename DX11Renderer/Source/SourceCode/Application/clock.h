//
//  Diploma of Interactive Gaming
//  Media Design School
//  Auckland
//  New Zealand
//
//  (c) 2012 Media Design School
//
//  File Name   :   clock.h
//  Description :   The game's clock object. Provides time keeping functionality.
//  Author      :   Team Green
//  Mail        :   tacticscubed@gmail.com
//

// TODO: Replace this with my newest clock.

#pragma once

#ifndef __IGAPR12_IG500_CLOCK_H__
#define __IGAPR12_IG500_CLOCK_H__

// Library Includes
#include <Windows.h>

// Local Includes
#include "types.h"

// Types

// Constants

// Prototypes

class Clock
{
    // Member Functions
	public:
		Clock();
		~Clock();

		void Process();

		Bool StartPerformanceTimer();
		Float64 StopPerformanceTimer();

		// Accessors
		// Get
		Float32 GetDeltaTick() const;

	private:
		Clock(const Clock& _krRHS);
		Clock operator =(const Clock& _krRHS);

    // Member Variables
	protected:
		// Constants
		const Float32 m_rMaxDeltaTick;

		// Variables
		Float64 m_rTimeElapsed;
		Float64 m_rDeltaTime;
		Float64 m_rTimerStartTime;
		Float64 m_rTimerEndTime;
		Float32 m_rLastTime;
		Float32 m_rCurrentTime;
		Bool m_bIsPerformanceTimerActive;

		// Windows Types
		LARGE_INTEGER m_CountsPerSecond;
		LARGE_INTEGER m_PerformanceTimer;
		LARGE_INTEGER m_LastTime;
		LARGE_INTEGER m_CurrentTime;
};

#endif // __IGAPR12_IG500_CLOCK_H__
