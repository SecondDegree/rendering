//
//  (c) 2015 James Hannam
//
//  File Name   :   testapplication.cpp
//  Description :   
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <Windows.h>
#include <SimpleMath.h>
#include <math.h>

// Local Includes
#include "utility.h"
#include "dx11renderer.h"
#include "dxcamera.h"
#include "dx11modeltextured.h"
#include "textureshader.h"
#include "window.h"

#include "unlitshader.h"

#include "dx11unlitmeshobject.h"

#include "squaremesh.h"
#include "trianglemesh.h"
#include "hexagonmesh.h"
#include "circlemesh.h"

// This Include
#include "testapplication.h"

using DirectX::SimpleMath::Matrix;

// Static Variables

// Static Function Prototypes

// Implementation
TestApplication::TestApplication(void)
{
}

TestApplication::~TestApplication(void)
{
	Shutdown();
}

Bool
TestApplication::InitialiseApplicationData(void)
{
	Bool bSucceeded = false;

	#if defined(DEBUG) || defined (_DEBUG)
		// Path to the working directory (for out of IDE and debug only).
		SetCurrentDirectory(L"..\\Bin\\");
	#endif // DEBUG || _DEBUG

	Bool bModelInitialised = false;
	Bool bModelTextureLoaded = false;

	m_pRenderer->SetClearColour(Colour::Grey());

	m_pCamera = std::make_unique<DXCamera>();
	if(m_pCamera->Initialise(70.0f, m_pWindow->GetClientWidthF(), m_pWindow->GetClientHeightF(), 0.01f, 100.0f))
	{
		m_pCamera->SetPos(0.0f, 0.0f, 5.0f);

		auto pDevice = m_pRenderer->GetDevice();
		auto pDeviceContext = m_pRenderer->GetDeviceContext();

		/*m_pModel = std::make_unique<DX11ModelTextured>();
		bModelInitialised = m_pModel->Initialise(pDevice, pDeviceContext);
		bModelTextureLoaded = m_pModel->LoadTexture(pDevice, L"assets\\textures\\sky.jpg");

		if(bModelInitialised && bModelTextureLoaded)
		{
			m_pTextureShader = std::make_unique<TextureShader>();

			bSucceeded = m_pTextureShader->Initialise(pDevice, pDeviceContext, m_pWindow->GetHandle());
		}*/

		m_pUnlitShader = std::make_unique<UnlitShader>();
		if(m_pUnlitShader->Initialise(pDevice, pDeviceContext, m_pWindow->GetHandle()))
		{
			m_pUnlitMeshObject = std::make_unique<DX11UnlitMeshObject>();

			auto pUnlitMesh = std::make_shared<HexagonMesh>();
			if(pUnlitMesh->Initialise(pDevice, pDeviceContext))
			{
				m_pUnlitMeshObject->SetMesh(pUnlitMesh);
				m_pUnlitMeshObject->SetColour(Colour::Orange());
			}
		}

		
	}

	return (bSucceeded);
}

void
TestApplication::Shutdown()
{

}

void
TestApplication::Process(Float32 _fDeltaTick)
{
	if(m_InputSubject.GetKeyState(KEY_ESCAPE) == KEYSTATE_DOWN)
	{
		PostQuitMessage(0);
	}

	if(m_pCamera != nullptr)
	{
		m_pCamera->Process(_fDeltaTick);
	}

	if(m_pUnlitMeshObject != nullptr)
	{
		static Float32 s_fCounter = 0.0f;
		s_fCounter += _fDeltaTick;

		m_pUnlitMeshObject->AddXPos(cosf(s_fCounter) * _fDeltaTick);
		m_pUnlitMeshObject->AddRoll((-cosf(s_fCounter)*2) * _fDeltaTick);

		m_pUnlitMeshObject->Process(_fDeltaTick);
	}
}

void
TestApplication::PreDraw(void)
{

}

void
TestApplication::Draw(void)
{
	if(m_pRenderer != nullptr)
	{
		const Matrix& world = m_pUnlitMeshObject->GetWorld();

		// Get the world, view and projection matrices from the camera and renderer.
		const Matrix& view = m_pCamera->GetView();
		const Matrix& proj = m_pCamera->GetProjection();

		Bool bAppliedShader = m_pUnlitShader->Apply(world, view, proj, m_pUnlitMeshObject->GetColour());

		if(bAppliedShader)
		{
			m_pUnlitMeshObject->Draw();
		}
	}
}

void
TestApplication::PostDraw(void)
{

}
