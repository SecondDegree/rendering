//
//  Bachelor of Software Engineering
//  Media Design School
//  Auckland
//  New Zealand
//
//  (c) 2013 Media Design School
//
//  File Name   :   inputsubject.cpp
//  Description :   Interprets input from the windows message pump and sends it to subscribers.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <cassert>

// Local Includes
#include "types.h"
#include "inputobserver.h"

// This Include
#include "inputsubject.h"

// Static Variables
Bool InputSubject::sm_bIsInitialised = false;
Int32 InputSubject::sm_iCursorXPosition = 0;
Int32 InputSubject::sm_iCursorYPosition = 0;
std::vector<InputObserver*> InputSubject::sm_Observers;
std::map<EKey, EKeyState> InputSubject::sm_KeyStates;

// Static Function Prototypes

// Implementation

/**
*
* This function builds the object
*
* @author James Hannam
*
*/
InputSubject::InputSubject()
{
}

/**
*
* This function destroys the object
*
* @author James Hannam
*
*/
InputSubject::~InputSubject()
{
}

/**
*
* This function initialises the key state map and
*	must be called before any other functions.
*
* @author James Hannam
*
*/
void
InputSubject::Initialise()
{
	// If the key state map has not been initialised.
	if(sm_bIsInitialised == false)
	{
		sm_bIsInitialised = true;
		
		for(UInt32 uiKey = INVALID_KEY + 1;
			uiKey < MAX_KEY;
			++uiKey)
		{
			// Initialise all keys to the unpressed state.
			sm_KeyStates[static_cast<EKey>(uiKey)] = KEYSTATE_UP;
		}
	}
}

/**
*
* This function resets all keys back to the unpressed
*	state and informs all observers of any change.
*
* @author James Hannam
*
*/
void
InputSubject::ResetKeys()
{
	for(UInt32 uiKey = INVALID_KEY + 1;
		uiKey < MAX_KEY;
		++uiKey)
	{
		EKey eCurrentKey = static_cast<EKey>(uiKey);

		// Set all keys to the unpressed state which are not already so.
		if(sm_KeyStates[eCurrentKey] != KEYSTATE_UP)
		{
			sm_KeyStates[eCurrentKey] = KEYSTATE_UP;

			if(sm_Observers.empty() == false)
			{
				// Notify each observer of the state change.
				std::vector<InputObserver*>::iterator iterObservers;
				for(iterObservers = sm_Observers.begin();
					iterObservers != sm_Observers.end();
					++iterObservers)
				{
					(*iterObservers)->OnKeyboardPress(eCurrentKey, KEYSTATE_UP);
				}
			}
		}
	} // End of key loop.
}

/**
*
* This function calls the keyboard press response
*	function on all subscribed observers.
*
* @author James Hannam
*
*/
void
InputSubject::FireKeyboardPress(WORD _wKey, const EKeyState _eKeyState)
{
	EKey eKey = ConvertToKey(_wKey);

	if(eKey != INVALID_KEY)
	{
		if(_eKeyState != sm_KeyStates[eKey])
		{
			sm_KeyStates[eKey] = _eKeyState;

			if(sm_Observers.empty() == false)
			{
				std::vector<InputObserver*>::iterator iterObservers;
				for(iterObservers = sm_Observers.begin();
					iterObservers != sm_Observers.end();
					++iterObservers)
				{
					(*iterObservers)->OnKeyboardPress(eKey, _eKeyState);
				}
			}
		}
	}
}

/**
*
* This function calls the mouse press response
*	function on all subscribed observers.
*
* @author James Hannam
*
*/
void
InputSubject::FireMousePress(const EKey _eMouseKey, const EKeyState _eKeyState)
{
	switch(_eMouseKey)
	{
		// Ensure that the provided key is a mouse key.
		case KEY_MOUSELEFT: // Fall through
		case KEY_MOUSEMIDDLE: // Fall through
		case KEY_MOUSERIGHT:
		{
			if(_eKeyState != sm_KeyStates[_eMouseKey])
			{
				sm_KeyStates[_eMouseKey] = _eKeyState;

				if(sm_Observers.empty() == false)
				{
					std::vector<InputObserver*>::iterator iterObserver;
					for(iterObserver = sm_Observers.begin();
						iterObserver != sm_Observers.end();
						++iterObserver)
					{
						(*iterObserver)->OnMousePress(_eMouseKey, _eKeyState);
					}
				}				
			}

			break;
		}
		default:
		{
			// Should not be hit.
			// The provided mouse key is not a mouse key.
			assert(false);
		}
	}
}

/**
*
* This function calls the mouse move response
*	function on all subscribed observers.
*
* @author James Hannam
*
*/
void
InputSubject::FireMouseMove(const Int32 _iXPosition, const Int32 _iYPosition)
{
	if(_iXPosition != sm_iCursorXPosition || _iYPosition != sm_iCursorYPosition)
	{
		sm_iCursorXPosition = _iXPosition;
		sm_iCursorYPosition = _iYPosition;
	
		if(sm_Observers.empty() == false)
		{
			std::vector<InputObserver*>::iterator iterObserver;
			for(iterObserver = sm_Observers.begin();
				iterObserver != sm_Observers.end();
				++iterObserver)
			{
				(*iterObserver)->OnMouseMove(sm_iCursorXPosition, sm_iCursorYPosition);
			}
		}
	}
}

/**
*
* This function calls the file drop response
*	function on all subscribed observers.
*
* @author James Hannam
*
*/
void
InputSubject::FireFileDrop(const Int8* const _piFilepath,
							const Int8* const _piFileExtension)
{
	if(sm_Observers.empty() == false)
	{
		std::vector<InputObserver*>::iterator iterObserver;
		for(iterObserver = sm_Observers.begin();
			iterObserver != sm_Observers.end();
			++iterObserver)
		{
			(*iterObserver)->OnFileDrop(_piFilepath, _piFileExtension);
		}
	}
}

/**
*
* This function adds the supplied observer to the observer vector.
*
* Returns false if the supplied observer is already subscribed or is invalid.
*
* @author James Hannam
*
*/
Bool
InputSubject::Subscribe(InputObserver* _In_ _pObserver)
{
	Bool bSuccess = false;

	if(_pObserver != 0)
	{
		if(sm_bIsInitialised)
		{
			Bool bIsSubscribed = false;

			if(sm_Observers.empty() == false)
			{
				// Iterate through the vector of observers.
				std::vector<InputObserver*>::iterator iterObserver;
				for(iterObserver = sm_Observers.begin();
					!(bIsSubscribed) && iterObserver != sm_Observers.end();
					++iterObserver)
				{
					// If the current observers pointer matches the supplied one.
					if((*iterObserver) == _pObserver)
					{
						bIsSubscribed = true;
					}
				}
			}

			// If the supplied observer is not already subscribed.
			if(bIsSubscribed == false)
			{
				sm_Observers.push_back(_pObserver);
				bSuccess = true;
			}
		}
		else
		{
			// Should not be hit.
			// The input subject has not been initialised.
			assert(false);
		}
	}

	return (bSuccess);
}

/**
*
* This function removes the supplied observer from the observer vector.
*
* Returns false if the supplied observer is not subscribed or is invalid.
*
* @author James Hannam
*
*/
Bool
InputSubject::Unsubscribe(InputObserver* _In_ _pObserver)
{
	Bool bSuccess = false;

	if(_pObserver != 0)
	{
		if(sm_bIsInitialised)
		{
			// Iterate through the vector of observers.
			std::vector<InputObserver*>::iterator iterObserver = sm_Observers.begin();
			while((bSuccess == false) && iterObserver != sm_Observers.end())
			{
				// If the current observers pointer matches the supplied one.
				if((*iterObserver) == _pObserver)
				{
					// Erase it from the vector.
					sm_Observers.erase(iterObserver);
					bSuccess = true;
				}
				else
				{
					++iterObserver;
				}
			}
		}
		else
		{
			// Should not be hit.
			// The input subject has not been initialised.
			assert(false);
		}
	}

	return (bSuccess);
}

// Accessors
// Get

/**
*
* This function gets the state of the supplied key.
*
* @author James Hannam
*
*/
EKeyState
InputSubject::GetKeyState(EKey _eKey)
{
	EKeyState eKeyState = INVALID_KEYSTATE;

	if(_eKey > INVALID_KEY && _eKey < MAX_KEY)
	{
		if(sm_bIsInitialised)
		{
			eKeyState = sm_KeyStates[_eKey];
		}
		else
		{
			// Should not be hit.
			// The input subject has not been initialised.
			assert(false);
		}
	}

	return (eKeyState);
}

/**
*
* This function gets cursors last recorded x position.
*
* @author James Hannam
*
*/
Int32
InputSubject::GetCursorXPosition()
{
	return (sm_iCursorXPosition);
}

/**
*
* This function gets cursors last recorded y position.
*
* @author James Hannam
*
*/
Int32
InputSubject::GetCursorYPosition()
{
	return (sm_iCursorYPosition);
}

// Private Functions

/**
*
* This function converts the provided key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not internally supported.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertToKey(const WORD _iKey)
{
	EKey eKey = INVALID_KEY;

	// If the key is a number.
	if(_iKey >= '0' && _iKey <= '9')
	{
		eKey = ConvertNumberToKey(_iKey);
	}
	// If the key is a function key.
	else if(_iKey >= VK_F1 && _iKey <= VK_F12)
	{
		eKey = ConvertFunctionToKey(_iKey);
	}
	// If the key is a character.
	else if(_iKey >= 'A' && _iKey <= 'Z')
	{
		eKey = ConvertCharacterToKey(_iKey);
	}
	// If the key is an arrow key.
	else if(_iKey >= VK_LEFT && _iKey <= VK_DOWN)
	{
		eKey = ConvertArrowToKey(_iKey);
	}
	// If the key is a number pad key.
	else if(_iKey >= VK_NUMPAD0 && _iKey <= VK_DIVIDE)
	{
		eKey = ConvertNumberpadToKey(_iKey);
	}
	else
	{
		eKey = ConvertOtherToKey(_iKey);
	}

	return (eKey);
}

/**
*
* This function converts the provided number key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not
*	internally supported or is not a number key.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertNumberToKey(const WORD _iNumber)
{
	EKey eKey = INVALID_KEY;

	switch(_iNumber)
	{
		case '0':
		{
			eKey = KEY_0;
			break;
		}
		case '1':
		{
			eKey = KEY_1;
			break;
		}
		case '2':
		{
			eKey = KEY_2;
			break;
		}
		case '3':
		{
			eKey = KEY_3;
			break;
		}
		case '4':
		{
			eKey = KEY_4;
			break;
		}
		case '5':
		{
			eKey = KEY_5;
			break;
		}
		case '6':
		{
			eKey = KEY_6;
			break;
		}
		case '7':
		{
			eKey = KEY_7;
			break;
		}
		case '8':
		{
			eKey = KEY_8;
			break;
		}
		case '9':
		{
			eKey = KEY_9;
			break;
		}
		default:
		{
			// Should not be hit.
			// The provided number is not a number key.
			assert(false);
			break;
		}
	}

	return (eKey);
}

/**
*
* This function converts the provided function key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not
*	internally supported or is not a function key.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertFunctionToKey(const WORD _iFunction)
{
	EKey eKey = INVALID_KEY;

	switch(_iFunction)
	{
		case VK_F1:
		{
			eKey = KEY_F1;
			break;
		}
		case VK_F2:
		{
			eKey = KEY_F2;
			break;
		}
		case VK_F3:
		{
			eKey = KEY_F3;
			break;
		}
		case VK_F4:
		{
			eKey = KEY_F4;
			break;
		}
		case VK_F5:
		{
			eKey = KEY_F5;
			break;
		}
		case VK_F6:
		{
			eKey = KEY_F6;
			break;
		}
		case VK_F7:
		{
			eKey = KEY_F7;
			break;
		}
		case VK_F8:
		{
			eKey = KEY_F8;
			break;
		}
		case VK_F9:
		{
			eKey = KEY_F9;
			break;
		}
		/*case VK_F10:
		{
			eKey = KEY_F10;
			break;
		}*/
		case VK_F11:
		{
			eKey = KEY_F11;
			break;
		}
		case VK_F12:
		{
			eKey = KEY_F12;
			break;
		}
		default:
		{
			// Should not be hit.
			// The provided function key is not a function key.
			assert(false);
			break;
		}
	}

	return (eKey);
}

/**
*
* This function converts the provided character key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not
*	internally supported or is not a character key.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertCharacterToKey(const WORD _iCharacter)
{
	EKey eKey = INVALID_KEY;

	switch(_iCharacter)
	{
		case 'A':
		{
			eKey = KEY_A;
			break;
		}
		case 'B':
		{
			eKey = KEY_B;
			break;
		}
		case 'C':
		{
			eKey = KEY_C;
			break;
		}
		case 'D':
		{
			eKey = KEY_D;
			break;
		}
		case 'E':
		{
			eKey = KEY_E;
			break;
		}
		case 'F':
		{
			eKey = KEY_F;
			break;
		}
		case 'G':
		{
			eKey = KEY_G;
			break;
		}
		case 'H':
		{
			eKey = KEY_H;
			break;
		}
		case 'I':
		{
			eKey = KEY_I;
			break;
		}
		case 'J':
		{
			eKey = KEY_J;
			break;
		}
		case 'K':
		{
			eKey = KEY_K;
			break;
		}
		case 'L':
		{
			eKey = KEY_L;
			break;
		}
		case 'M':
		{
			eKey = KEY_M;
			break;
		}
		case 'N':
		{
			eKey = KEY_N;
			break;
		}
		case 'O':
		{
			eKey = KEY_O;
			break;
		}
		case 'P':
		{
			eKey = KEY_P;
			break;
		}
		case 'Q':
		{
			eKey = KEY_Q;
			break;
		}
		case 'R':
		{
			eKey = KEY_R;
			break;
		}
		case 'S':
		{
			eKey = KEY_S;
			break;
		}
		case 'T':
		{
			eKey = KEY_T;
			break;
		}
		case 'U':
		{
			eKey = KEY_U;
			break;
		}
		case 'V':
		{
			eKey = KEY_V;
			break;
		}
		case 'W':
		{
			eKey = KEY_W;
			break;
		}
		case 'X':
		{
			eKey = KEY_X;
			break;
		}
		case 'Y':
		{
			eKey = KEY_Y;
			break;
		}
		case 'Z':
		{
			eKey = KEY_Z;
			break;
		}
		default:
		{
			// Should not be hit.
			// The provided character is not a character key.
			assert(false);
			break;
		}
	}

	return (eKey);
}

/**
*
* This function converts the provided arrow key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not
*	internally supported or is not an arrow key.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertArrowToKey(const WORD _iArrow)
{
	EKey eKey = INVALID_KEY;

	switch(_iArrow)
	{
		case VK_LEFT:
		{
			eKey = KEY_ARROWLEFT;
			break;
		}
		case VK_UP:
		{
			eKey = KEY_ARROWUP;
			break;
		}
		case VK_RIGHT:
		{
			eKey = KEY_ARROWRIGHT;
			break;
		}
		case VK_DOWN:
		{
			eKey = KEY_ARROWDOWN;
			break;
		}
		default:
		{
			// Should not be hit.
			// The provided character is not a character.
			assert(false);
			break;
		}
	}

	return (eKey);
}

/**
*
* This function converts the provided number pad key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not
*	internally supported or is not a number pad key.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertNumberpadToKey(const WORD _iNumberpad)
{
	EKey eKey = INVALID_KEY;

	switch(_iNumberpad)
	{
		case VK_NUMPAD0:
		{
			eKey = KEY_NUM0;
			break;
		}
		case VK_NUMPAD1:
		{
			eKey = KEY_NUM1;
			break;
		}
		case VK_NUMPAD2:
		{
			eKey = KEY_NUM2;
			break;
		}
		case VK_NUMPAD3:
		{
			eKey = KEY_NUM3;
			break;
		}
		case VK_NUMPAD4:
		{
			eKey = KEY_NUM4;
			break;
		}
		case VK_NUMPAD5:
		{
			eKey = KEY_NUM5;
			break;
		}
		case VK_NUMPAD6:
		{
			eKey = KEY_NUM6;
			break;
		}
		case VK_NUMPAD7:
		{
			eKey = KEY_NUM7;
			break;
		}
		case VK_NUMPAD8:
		{
			eKey = KEY_NUM8;
			break;
		}
		case VK_NUMPAD9:
		{
			eKey = KEY_NUM9;
			break;
		}
		case VK_MULTIPLY:
		{
			eKey = KEY_MULTIPLY;
			break;
		}
		case VK_ADD:
		{
			eKey = KEY_ADD;
			break;
		}
		case VK_SEPARATOR:
		{
			eKey = KEY_SEPARATOR;
			break;
		}
		case VK_SUBTRACT:
		{
			eKey = KEY_SUBTRACT;
			break;
		}
		case VK_DECIMAL:
		{
			eKey = KEY_DECIMAL;
			break;
		}
		case VK_DIVIDE:
		{
			eKey = KEY_DIVIDE;
			break;
		}
		default:
		{
			// Should not be hit.
			// The provided number pad key is not a number pad key.
			assert(false);
			break;
		}
	}

	return (eKey);
}

/**
*
* This function converts the provided key (as a WORD) to an EKey.
*
* Returns an invalid key if the supplied key is not internally supported.
*
* @author James Hannam
*
*/
EKey
InputSubject::ConvertOtherToKey(const WORD _iOther)
{
	EKey eKey = INVALID_KEY;

	switch(_iOther)
	{
		case VK_ESCAPE:
		{
			eKey = KEY_ESCAPE;
			break;
		}
		case VK_SPACE:
		{
			eKey = KEY_SPACE;
			break;
		}
		case VK_RETURN:
		{
			eKey = KEY_ENTER;
			break;
		}
		case VK_BACK:
		{
			eKey = KEY_BACKSPACE;
			break;
		}
		case VK_SHIFT:
		{
			eKey = KEY_SHIFT;
			break;
		}
		case VK_CONTROL:
		{
			eKey = KEY_CONTROL;
			break;
		}
		case VK_TAB:
		{
			eKey = KEY_TAB;
			break;
		}
		case VK_HOME:
		{
			eKey = KEY_HOME;
			break;
		}
		case VK_END:
		{
			eKey = KEY_END;
			break;
		}
		case VK_INSERT:
		{
			eKey = KEY_INSERT;
			break;
		}
		case VK_DELETE:
		{
			eKey = KEY_DELETE;
			break;
		}
		case VK_PRIOR:
		{
			eKey = KEY_PAGEUP;
			break;
		}
		case VK_NEXT:
		{
			eKey = KEY_PAGEDOWN;
			break;
		}
		case VK_OEM_PLUS:
		{
			eKey = KEY_PLUS;
			break;
		}
		case VK_OEM_MINUS:
		{
			eKey = KEY_MINUS;
			break;
		}
		case VK_OEM_COMMA:
		{
			eKey = KEY_COMMA;
			break;
		}
		case VK_OEM_PERIOD:
		{
			eKey = KEY_PERIOD;
			break;
		}
		case VK_OEM_1:
		{
			eKey = KEY_COLON;
			break;
		}
		case VK_OEM_7:
		{
			eKey = KEY_QUOTE;
			break;
		}
		case VK_OEM_4:
		{
			eKey = KEY_BRACKETOPEN;
			break;
		}
		case VK_OEM_6:
		{
			eKey = KEY_BRACKETCLOSE;
			break;
		}
		case VK_OEM_3:
		{
			eKey = KEY_TILD;
			break;
		}
		case VK_OEM_2:
		{
			eKey = KEY_FORWARDSLASH;
			break;
		}
		case VK_OEM_5:
		{
			eKey = KEY_BACKSLASH;
			break;
		}
		default:
		{
			// The provided key is not a supported key.
			break;
		}
	}

	return (eKey);
}