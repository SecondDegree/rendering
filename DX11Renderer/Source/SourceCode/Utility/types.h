//
//  (c) 2013-2014 James Hannam
//
//  File Name   :   types.h
//  Description :   Type defines types with new, more explicit/compact names.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Intrinsic Types
typedef bool			 Bool;
typedef char			 Int8;
typedef unsigned char	 UInt8;
typedef short			 Int16;
typedef unsigned short	 UInt16;
typedef int				 Int32;
typedef unsigned int	 UInt32;
typedef __int64			 Int64;
typedef unsigned __int64 UInt64;
typedef float			 Float32;
typedef double			 Float64;

// TODO: !!! LEGACY 2013 - Needs review. !!!

/* 
* Coding Standard.
* The following is the coding standard to be used for this codebase.
*
* Naming:
*	Hungarian Notation:
*		i - integer; used for all integer types (Int8-Int64) including unsigned.
*		r - real; (real number) used for all floating point types.
*		b - bool.
*		p - pointer; to be used in conjunction with other hungarian notations where appropriate.
*		s - string; can be a std::string or a char pointer (which should be noted).
*		e - enumeration; used for all enumerated types.
*		m - member; used for all class wide member variables (followed by an underscore (_)).
*		s - static; used for all static variables (followed by an underscore (_)
						or m (for static member variables)).
*
*		The static notation should precede all other notations
*			(i.e. sm_bIsFullScreen) such that it reads "static member".
*		The pointer notation should precede all other notations which follow an underscore
			(i.e. m_piIndices) such that it reads like "pointer to an integer".
*
*	All variables are to be named using camel case (camelCase).
*	All functions are to be named using pascal case (PascalCase).
*	All parameters are to be preceded with an underscore (_).
*	All member variables are to be preceded with a lower case m followed by an underscore (m_).
*	Parameters and member variables should be named using pascal
		case following their above defined prefixes.
* 
* SAL Notation:
*	SAL notation is to be used for all pointers to provide more information on how they will be used.
*
*	Const pointers should passed when data alteration is forbidden.
*
*	_In_ - The data being pointed to will be read from/written to or stored for later reading/writing.
*	_Inout_ - The data being pointed to will be modified locally for use outside.
*	_Out_ - The pointer will be modified to point to the correct data.
*/