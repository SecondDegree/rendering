//
//  (c) 2014-15 James Hannam
//
//  File Name   :   utility.h
//  Description :   A grouping of useful functions/macros.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes

// Local Includes
#include "types.h"

// Types

// Constants

// Prototypes

// Macros
/*
* Calls Release() on the pointer p if it is valid then nulls it.
*/
#define ReleaseCOMNull(_p)\
	if(_p != nullptr)\
	{\
		_p->Release();\
		_p = nullptr;\
	}

/*
* Deletes p then nulls it.
*/
#define DeleteNull(_p)\
	delete _p;\
	_p = nullptr;

/*
* Deletes array p then nulls it.
*/
#define DeleteArrayNull(_p)\
	delete[] _p;\
	_p = nullptr;

// TODO: ^ Remove these above functions and use com pointers/shared pointers.

/*
* Gets the size (number of elements) of the array _a.
* NOTE: Only works on stack arrays!!.
*/
#define ArraySize(_a) (sizeof(_a) / sizeof(_a[0]));

/*
* Wrapper for pragma comment(lib, _lib ".lib").
* Adds ".lib" after the provided _lib.
*/
#define PragmaLib(_lib)\
	__pragma(comment(lib, _lib ".lib"))

/*
* Same as PragmaLib but adds "_d.lib" instead of ".lib" after _lib in debug mode.
*/
#if defined(_DEBUG) || defined(DEBUG)
	#define PragmaLib_d(_lib)\
		__pragma(comment(lib, _lib "_d.lib"))
#else
	#define PragmaLib_d(_lib)\
		__pragma(comment(lib, _lib ".lib"))
#endif // defined(_DEBUG) || defined(DEBUG)

// Inline functions

#include "utility.inl"