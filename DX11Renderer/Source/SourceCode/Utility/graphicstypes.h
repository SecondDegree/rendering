//
//  (c) 2015 James Hannam
//
//  File Name   :   graphicstypes.h
//  Description :   Type definitions for graphics (rendering)
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once