//
//  (c) 2015 James Hannam
//
//  File Name   :   mathutility.inl
//  Description :   Useful math functions and definitions
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Library Includes
#include <math.h>

// Local Includes

// Implementation
namespace MathUtility
{
	template<class _T>
	_T
	Wrap(const _T& _Value,
		 const _T& _Step,
		 const _T& _RangeMin,
		 const _T& _RangeMax)
	{
		_T value = _Value;

		while(value >= _RangeMax)
		{
			value -= _Step;
		}

		while(value <= _RangeMin)
		{
			value += _Step;
		}

		return (value);
	}

	template<class _T>
	_T
	Wrap(const _T& _Value, const _T& _Step)
	{
		return (Wrap(_Value, _Step, (-_Step), _Step));
	}

	Float32
	WrapRadians(Float32 _fValue)
	{
		return (Wrap(_fValue, Pi::Tau, 0.0f, Pi::Tau));
	}
	
	void
	WrapRadiansRef(Float32& _fValue)
	{
		_fValue = WrapRadians(_fValue);
	}

	Float32
	WrapDegrees(Float32 _fValue)
	{
		return (Wrap(_fValue, 360.0f, 0.0f, 360.0f));
	}

	void
	WrapDegreesRef(Float32& _fValue)
	{
		_fValue = WrapDegrees(_fValue);
	}

	template<class _T>
	_T
	Clamp(const _T& _Value, const _T& _Min, const _T& _Max)
	{
		if(_Value < _Min)
		{
			return (_Min);
		}
		else if(_Value > _Max)
		{
			return (_Max);
		}

		return (_Value);
	}

	template<class _T>
	void
	ClampRef(_T& _Value, const _T& _Min, const _T& _Max)
	{
		_Value = Clamp(_Value, _Min, _Max);
	}
}