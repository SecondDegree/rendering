//
//  (c) 2015 James Hannam
//
//  File Name   :   mathutility.h
//  Description :   Useful math functions and definitions
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Library Includes
#include <DirectXMath.h>

// Local Includes
#include "types.h"

// Macros
// Rotation macros

/* DirectX Tool Kit defines it's world axes as such:
*  X -  Vertical axis	- Positive is Up
*  Y -  Horizontal axis	- Positive is Right
*  Z -  Forward axis	- Positive is Backward
*  NOTE: This is a right hand system!!
*/

// Extracts the Yaw component from vector _v
#define Yaw(_v)		_v.x

// Extracts the Pitch component from vector _v
#define Pitch(_v)	_v.y

// Extracts the Roll component from vector _v
#define Roll(_v)	_v.z

namespace MathUtility
{
	// Constants
	namespace Pi
	{
		using namespace DirectX;

		const Float32 Pi = XM_PI;
		const Float32 Tau = XM_2PI; // Pi*2
		const Float32 TwoPi = Tau; // Pi*2
		const Float32 OneDivPi = XM_1DIVPI; // 1/Pi
		const Float32 OneDivTau = XM_1DIV2PI; // 1/(2Pi)
		const Float32 OneDivTwoPi = OneDivTau; // 1/(2Pi)
		const Float32 HalfPi = XM_PIDIV2; // Pi/2
		const Float32 QuarterPi = XM_PIDIV4; // Pi/4
	};

	// Functions
	/*
	* Wrap
	* @desc - Wraps the given _Value by _Step until it is within (_RangeMin - _RangeMax) range.
	*/
	template<class _T>
	inline _T Wrap(const _T& _Value,
				   const _T& _Step,
				   const _T& _RangeMin,
				   const _T& _RangeMax);

	/*
	* Wrap
	* @desc - Wraps the given _Value by _Step until it is within (-_Step) - _Step range.
	*/
	template<class _T>
	inline _T Wrap(const _T& _Value, const _T& _Step);

	/*
	* Wrap Radians
	* @desc - Wraps _fValue to be within 0-2Pi radians.
	* @return - The result of wrapping _fValue.
	*/
	inline Float32 WrapRadians(Float32 _fValue);

	/*
	* WrapRadiansRef
	* @desc - Sets _fValue to be the wrapped value of _fValue within 0-2Pi radians.
	*/
	inline void WrapRadiansRef(Float32& _fValue);

	/*
	* WrapDegrees
	* @desc - Wraps _fValue to be within 0-360 degrees.
	* @return - The result of wrapping _fValue.
	*/
	inline Float32 WrapDegrees(Float32 _fValue);

	/*
	* WrapDegrees
	* @desc - Sets _fValue to be the wrapped value of _fValue within 0-360 degrees.
	*/
	inline void WrapDegreesRef(Float32& _fValue);

	/*
	* Clamp
	* @desc - Clamps _Value to be no less than _Min or no more than _Max.
	* @return - The resulting value from clamping _Value.
	*/
	template<class _T>
	inline _T Clamp(const _T& _Value, const _T& _Min, const _T& _Max);

	/*
	* Clamp
	* @desc - Sets _Value to be no less than _Min or no more than _Max.
	*/
	template<class _T>
	inline void ClampRef(_T& _Value, const _T& _Min, const _T& _Max);
}

#include "mathutility.inl"