//
//  (c) 2015 James Hannam
//
//  File Name   :   dxobject.h
//  Description :   Base class for all objects which can exist within the
//						scene. Provides positional functionality.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

#pragma once

// Preprocessor Directives

// Library Includes
#include <SimpleMath.h>

// Local Includes
#include "types.h"

// Types

// Constants

// Prototypes

using DirectX::SimpleMath::Vector3;
using DirectX::SimpleMath::Matrix;

class DXObject abstract
{
    // Member Functions
	protected:
		DXObject();

	public:
		virtual ~DXObject();

		virtual void Process(Float32 _fDeltaTick);

		// TODO: Add additional functionality:
			// Forward, Right and Up vectors.

		// Accessors
		// Get
		// Matrix
		// TODO: Figure out how to solve the 1 frame delay issue this method of
			// updating the world matrix only at the end of each process step causes.
		const Matrix& GetWorld(void) const;

		// Position
		const Vector3& GetPos(void) const;
		Float32 GetXPos(void) const;
		Float32 GetYPos(void) const;
		Float32 GetZPos(void) const; 

		// Rotation
		const Vector3& GetRot(void) const;
		Float32 GetYaw(void) const;
		Float32 GetPitch(void) const;
		Float32 GetRoll(void) const;

		// Scale
		const Vector3& GetScale(void) const;
		Float32 GetXScale(void) const;
		Float32 GetYScale(void) const;
		Float32 GetZScale(void) const;

		// Set
		// Position
		void SetPos(const Vector3& _NewPos);
		void SetPos(Float32 _NewXPos, Float32 _NewYPos, Float32 _NewZPos);
		void SetXPos(Float32 _NewXPos);
		void SetYPos(Float32 _NewYPos);
		void SetZPos(Float32 _NewZPos);

		void AddPos(const Vector3& _Offset);
		void AddPos(Float32 _XOffset, Float32 _YOffset, Float32 _ZOffset);
		void AddXPos(Float32 _Offset);
		void AddYPos(Float32 _Offset);
		void AddZPos(Float32 _Offset);

		// Rotation
		void SetRot(const Vector3& _NewRot);
		void SetRot(Float32 _NewYaw, Float32 _NewPitch, Float32 _NewRoll);
		void SetYaw(Float32 _NewYaw);
		void SetPitch(Float32 _NewPitch);
		void SetRoll(Float32 _NewRoll);

		void AddRot(const Vector3& _Offset);
		void AddRot(Float32 _YawOffset, Float32 _PitchOffset, Float32 _RollOffset);
		void AddYaw(Float32 _Offset);
		void AddPitch(Float32 _Offset);
		void AddRoll(Float32 _Offset);

		// Scale
		void SetScale(const Vector3& _NewScale);
		void SetScale(Float32 _NewXScale, Float32 _NewYScale, Float32 _NewZScale);
		void SetXScale(Float32 _NewXScale);
		void SetYScale(Float32 _NewYScale);
		void SetZScale(Float32 _NewZScale);

		void AddScale(const Vector3& _Offset);
		void AddScale(Float32 _XOffset, Float32 _YOffset, Float32 _ZOffset);
		void AddXScale(Float32 _Offset);
		void AddYScale(Float32 _Offset);
		void AddZScale(Float32 _Offset);

	protected:
		// Events
		virtual void OnRecalculateWorld(void) {};

	// Private Functions
	private:
		void RecalculateWorld(void);

    // Member Variables
	private:
		Matrix m_World;

							//    X		  Y		 Z		//
		Vector3 m_Position; //	Forward	Right	Up		//
		Vector3 m_Rotation; //	Yaw		Pitch	Roll	// Stored in radians
		Vector3 m_Scale;

		Bool m_bRecalculateWorld;
};	
