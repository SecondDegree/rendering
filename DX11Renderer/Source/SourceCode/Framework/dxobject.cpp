//
//  (c) 2015 James Hannam
//
//  File Name   :   dxobject.cpp
//  Description :   Base class for all objects which can exist within the
//						scene. Provides positional functionality.
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Preprocessor Directives

// Library Includes

// Local Includes
#include "mathutility.h"

// This Include
#include "dxobject.h"

// Static Variables

// Prototypes

// Implementation

DXObject::DXObject()
: m_Scale(Vector3::One)
, m_bRecalculateWorld(false)
{
	RecalculateWorld();
}

DXObject::~DXObject()
{

}

void
DXObject::Process(Float32 _fDeltaTick)
{
	RecalculateWorld();
}

// Accessors
// Get
// Matrix
const Matrix&
DXObject::GetWorld(void) const
{
	return (m_World);
}

// Position
const Vector3&
DXObject::GetPos(void) const
{
	return (m_Position);
}

Float32
DXObject::GetXPos(void) const
{
	return (m_Position.x);
}

Float32
DXObject::GetYPos(void) const
{
	return (m_Position.y);
}

Float32
DXObject::GetZPos(void) const
{
	return (m_Position.z);
}

// Rotation
const Vector3&
DXObject::GetRot(void) const
{
	return (m_Rotation);
}

Float32
DXObject::GetYaw(void) const
{
	return (Yaw(m_Rotation));
}

Float32
DXObject::GetPitch(void) const
{
	return (Pitch(m_Rotation));
}

Float32
DXObject::GetRoll(void) const
{
	return (Roll(m_Rotation));
}

// Scale
const Vector3&
DXObject::GetScale(void) const
{
	return (m_Scale);
}

Float32
DXObject::GetXScale(void) const
{
	return (m_Scale.x);
}

Float32
DXObject::GetYScale(void) const
{
	return (m_Scale.y);
}

Float32
DXObject::GetZScale(void) const
{
	return (m_Scale.z);
}

// Set
// Position
void
DXObject::SetPos(const Vector3& _NewPos)
{
	m_Position = _NewPos;
	m_bRecalculateWorld = true;
}

void
DXObject::SetPos(Float32 _NewXPos, Float32 _NewYPos, Float32 _NewZPos)
{
	m_Position.x = _NewXPos;
	m_Position.y = _NewYPos;
	m_Position.z = _NewZPos;
	m_bRecalculateWorld = true;
}

void
DXObject::SetXPos(Float32 _NewXPos)
{
	m_Position.x = _NewXPos;
	m_bRecalculateWorld = true;
}

void
DXObject::SetYPos(Float32 _NewYPos)
{
	m_Position.y = _NewYPos;
	m_bRecalculateWorld = true;
}

void
DXObject::SetZPos(Float32 _NewZPos)
{
	m_Position.z = _NewZPos;
	m_bRecalculateWorld = true;
}

void
DXObject::AddPos(const Vector3& _Offset)
{
	SetPos(GetPos() + _Offset);
}

void
DXObject::AddPos(Float32 _XOffset, Float32 _YOffset, Float32 _ZOffset)
{
	SetPos((GetXPos() + _XOffset), (GetYPos() + _YOffset), (GetZPos() + _ZOffset));
}

void
DXObject::AddXPos(Float32 _Offset)
{
	SetXPos(GetXPos() + _Offset);
}

void
DXObject::AddYPos(Float32 _Offset)
{
	SetYPos(GetYPos() + _Offset);
}

void
DXObject::AddZPos(Float32 _Offset)
{
	SetZPos(GetZPos() + _Offset);
}

// Rotation
void
DXObject::SetRot(const Vector3& _NewRot)
{
	SetYaw(Yaw(_NewRot));
	SetPitch(Pitch(_NewRot));
	SetRoll(Roll(_NewRot));
}

void
DXObject::SetRot(Float32 _NewYaw, Float32 _NewPitch, Float32 _NewRoll)
{
	SetYaw(_NewYaw);
	SetPitch(_NewPitch);
	SetRoll(_NewRoll);
}

void
DXObject::SetYaw(Float32 _NewYaw)
{
	Yaw(m_Rotation) = MathUtility::WrapRadians(_NewYaw);
	m_bRecalculateWorld = true;
}

void
DXObject::SetPitch(Float32 _NewPitch)
{
	Pitch(m_Rotation) = MathUtility::WrapRadians(_NewPitch);
	m_bRecalculateWorld = true;
}

void
DXObject::SetRoll(Float32 _NewRoll)
{
	Roll(m_Rotation) = MathUtility::WrapRadians(_NewRoll);
	m_bRecalculateWorld = true;
}

void
DXObject::AddRot(const Vector3& _Offset)
{
	SetRot(GetRot() + _Offset);
}

void
DXObject::AddRot(Float32 _YawOffset, Float32 _PitchOffset, Float32 _RollOffset)
{
	SetRot((GetYaw() + _YawOffset), (GetPitch() + _PitchOffset), (GetRoll() + _RollOffset));
}

void
DXObject::AddYaw(Float32 _Offset)
{
	SetYaw(GetYaw() + _Offset);
}

void
DXObject::AddPitch(Float32 _Offset)
{
	SetPitch(GetPitch() + _Offset);
}

void
DXObject::AddRoll(Float32 _Offset)
{
	SetRoll(GetRoll() + _Offset);
}

// Scale
void
DXObject::SetScale(const Vector3& _NewScale)
{
	m_Scale = _NewScale;
	m_bRecalculateWorld = true;
}

void
DXObject::SetScale(Float32 _NewXScale, Float32 _NewYScale, Float32 _NewZScale)
{
	m_Scale.x = _NewXScale;
	m_Scale.y = _NewYScale;
	m_Scale.z = _NewZScale;
	m_bRecalculateWorld = true;
}

void
DXObject::SetXScale(Float32 _NewXScale)
{
	m_Scale.x = _NewXScale;
	m_bRecalculateWorld = true;
}

void
DXObject::SetYScale(Float32 _NewYScale)
{
	m_Scale.y = _NewYScale;
	m_bRecalculateWorld = true;
}

void
DXObject::SetZScale(Float32 _NewZScale)
{
	m_Scale.z = _NewZScale;
	m_bRecalculateWorld = true;
}

void
DXObject::AddScale(const Vector3& _Offset)
{
	SetScale(GetScale() + _Offset);
}

void
DXObject::AddScale(Float32 _XOffset, Float32 _YOffset, Float32 _ZOffset)
{
	SetScale((GetXScale() + _XOffset), (GetYScale() + _YOffset), (GetZScale() + _ZOffset));
}

void
DXObject::AddXScale(Float32 _Offset)
{
	SetXScale(GetXScale() + _Offset);
}

void
DXObject::AddYScale(Float32 _Offset)
{
	SetYScale(GetYScale() + _Offset);
}

void
DXObject::AddZScale(Float32 _Offset)
{
	SetZScale(GetZScale() + _Offset);
}

void
DXObject::RecalculateWorld(void)
{
	if(m_bRecalculateWorld)
	{
		// S * R * T = world - right hand rule.

		m_World = (Matrix::CreateScale(GetScale()) *
				   // This is a quaternion rotation under the hood.
				   Matrix::CreateFromYawPitchRoll(GetYaw(), GetPitch(), GetRoll()) *
				   Matrix::CreateTranslation(GetPos()));
		m_bRecalculateWorld = false;

		OnRecalculateWorld();
	}
}