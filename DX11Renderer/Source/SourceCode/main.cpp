//
//  (c) 2014-15 James Hannam
//
//  File Name   :   main.cpp
//  Description :   The entry point of the application
//  Author      :   James Hannam
//  Mail        :   jamesahannam@gmail.com
//

// Pre-Processor Directives
#define WIN32_LEAN_AND_MEAN

#ifndef _WIN32_WINNT
	#define _WIN32_WINNT 0x0601 // Windows 7 and up are the target platforms.
#endif // _WIN32_WINNT

// Library Includes
#include <Windows.h>
#include <vld.h>
#include <memory>

// Local Includes
#include "types.h"
#include "testapplication.h"
#include "inputsubject.h"

// Implementation
Int32 WINAPI WinMain(HINSTANCE _pInstance,
					 HINSTANCE _pPreviousInstance,
					 PSTR _psCMDLine,
					 Int32 _iCMDShow)
{
	Int32 iExitCode = 0;
	Bool bRun = false;

	// Initialise the input subject.
	InputSubject::Initialise();

	// Create and initialise the window.
	auto pApplication = std::make_unique<TestApplication>();

	if(pApplication != nullptr)
	{
		bRun = pApplication->Initialise(_pInstance, L"DX11 Renderer");

		if(bRun)
		{
			pApplication->Run();
		}
	}
	else
	{

	}

	return (iExitCode);
}